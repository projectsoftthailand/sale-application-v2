import React from "react";
import { Dimensions, StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import normalize from "../assets/normalize.js";
import color from "../statics/color";

const { width } = Dimensions.get("window");

export default class HeadBar extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.eng}>{this.props.eng.toUpperCase()}</Text>
        {typeof this.props.visible === "undefined" ||
        this.props.visible === true ? (
          <Text style={styles.th}>{this.props.th}</Text>
        ) : null}
        <Icon name="sort-down" size={normalize(10)} color="white" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.Main,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: width,
    height: normalize(60)
  },
  eng: {
    fontSize: normalize(16),
    marginVertical: normalize(-1.5),
    color: "white"
  },
  th: {
    fontSize: normalize(10),
    marginVertical: normalize(-1.5),
    color: "white"
  }
});
