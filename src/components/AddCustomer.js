import React from "react";
import { TouchableOpacity, Image } from "react-native";
import normalize from "../assets/normalize.js";

export default class AddCustomer extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate("CareAdd")}
        style={{
          marginRight: normalize(10),
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Image
          source={require("../assets/icon_mobile/add_customer.png")}
          style={{
            height: normalize(30),
            width: normalize(30),
            resizeMode: "contain",
            zIndex: 99
          }}
        />
      </TouchableOpacity>
    );
  }
}
