import React from "react";
import { TouchableOpacity, Image } from "react-native";
import normalize from "../assets/normalize.js";
import Back from "../assets/icon_mobile/arrow_left.png";

export default class HeaderBackButton extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity
        style={{
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center"
        }}
        onPress={() => this.props.navigation.goBack()}
      >
        <Image
          source={Back}
          style={{
            margin: normalize(15),
            height: normalize(25),
            width: normalize(25),
            resizeMode: "contain"
          }}
        />
      </TouchableOpacity>
    );
  }
}
