import React, { Component } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  Alert,
  View,
  Text
} from "react-native";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";

import normalize from "../assets/normalize";
import color from "../statics/color";
import { post } from "../services";
import font from "../assets/font";
import User from "../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class ModalEditStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    };
  }

  onSelect(index, value) {
    this.setState({
      value: value
    });
  }

  _updateStatus = async () => {
    let object = {
      sale_id: User.user_id,
      interest_id: this.props.interest_id,
      status: this.state.value !== null ? this.state.value : this.props.status
    };
    await post("update_interest_status", object, User.token).then(res => {
      this.props.dismissModal();
      this.props._fetch();
      setTimeout(() => {
        Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
      }, 200);
    });
  };

  render() {
    const { visible } = this.props;
    return (
      <Modal
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          console.log("Modal has been closed.");
        }}
      >
        <TouchableOpacity
          style={styles.backgroundModal}
          onPress={() => this.props.dismissModal()}
        />

        <View style={styles.modal}>
          <View style={styles.head}>
            <Text style={styles.headText}>แก้ไขสถานะ</Text>
          </View>

          <RadioGroup
            selectedIndex={this.props.status - 1}
            onSelect={(index, value) => this.onSelect(index, value)}
          >
            <RadioButton style={styles.item} value={1}>
              <Text style={styles.label}>เร่งปิดการขาย</Text>
            </RadioButton>

            <RadioButton style={styles.item} value={2}>
              <Text style={styles.label}>การขายปานกลาง</Text>
            </RadioButton>

            <RadioButton style={styles.item} value={3}>
              <Text style={styles.label}>การขายปกติ</Text>
            </RadioButton>

            <RadioButton style={styles.item} value={0}>
              <Text style={styles.label}>ปิดการขาย</Text>
            </RadioButton>
          </RadioGroup>

          <TouchableOpacity
            style={styles.buttonBlue}
            onPress={() => this._updateStatus()}
          >
            <Text style={styles.buttonText}>ตกลง</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  backgroundModal: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    position: "absolute",
    height: height,
    width: width,
    zIndex: 0
  },
  modal: {
    padding: normalize(10),
    width: aspectRatio < 1.6 ? width * 0.65 : width * 0.75,
    top: aspectRatio < 1.6 ? height * 0.25 : height * 0.2,
    left: aspectRatio < 1.6 ? width * 0.175 : width * 0.125,
    backgroundColor: "white",
    justifyContent: "center",
    position: "absolute",
    borderRadius: normalize(3),
    zIndex: 1
  },
  head: {
    height: normalize(25),
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  headText: {
    fontSize: normalize(16),
    color: color.Main,
    fontFamily: font.bold
  },
  item: {
    alignItems: "center",
    borderColor: "lightgrey",
    borderBottomWidth: normalize(0.5)
  },
  buttonBlue: {
    width: aspectRatio < 1.6 ? width * 0.65 : width * 0.75,
    borderBottomRightRadius: normalize(3),
    borderBottomLeftRadius: normalize(3),
    backgroundColor: color.Main,
    bottom: normalize(-10),
    padding: normalize(15),
    alignItems: "center",
    alignSelf: "center"
  },
  buttonText: {
    fontSize: normalize(16),
    color: "white"
  },
  label: {
    marginLeft: normalize(10),
    color: color.SubMain,
    fontSize: normalize(14)
  }
});
