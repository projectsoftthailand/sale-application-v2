import React from "react";
import { Dimensions, StyleSheet, Image, Text } from "react-native";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

import color from "../statics/color";
import normalize from "../assets/normalize.js";

import ModalEditCategory from "./ModalEditCategory";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class AddProduct extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false
    };
  }
  _showModal() {
    this.setState({
      showModal: true
    });
  }

  _dismissModal() {
    this.setState({
      showModal: false
    });
  }

  render() {
    return (
      <Menu>
        <MenuTrigger
          style={{
            marginRight: normalize(10),
            alignItems: "center",
            justifyContent: "center"
          }}
          text={null}
          disabled={this.props.disabled}
        >
          <Image
            source={require("../assets/icon_mobile/add_product.png")}
            style={{
              height: normalize(30),
              width: normalize(30),
              resizeMode: "contain",
              zIndex: 99
            }}
          />
        </MenuTrigger>
        <MenuOptions
          optionsContainerStyle={{
            marginTop: normalize(25),
            marginLeft: aspectRatio < 1.6 ? -(width * 0.125) : -(width * 0.025)
          }}
        >
          <MenuOption
            onSelect={() => this.props.navigation.navigate("ProductAdd")}
            style={styles.Option}
          >
            <Text style={styles.textMenu}>เพิ่มสินค้า</Text>
          </MenuOption>
          <MenuOption onSelect={() => this._showModal()} style={styles.Option}>
            <Text style={styles.textMenu}>แก้ไขประเภทสินค้า</Text>
          </MenuOption>
        </MenuOptions>
        <ModalEditCategory
          visible={this.state.showModal}
          dismissModal={this._dismissModal.bind(this)}
        />
      </Menu>
    );
  }
}

const styles = StyleSheet.create({
  textMenu: {
    fontSize: normalize(14),
    padding: normalize(10),
    color: color.SubMain
  },
  Option: {
    width: aspectRatio < 1.6 ? width * 0.3 : null,
    backgroundColor: "white",
    borderColor: "lightgrey",
    borderWidth: normalize(0.5)
  }
});
