import React from "react";
import { StyleSheet, ScrollView, Image, View, Text } from "react-native";
import { DrawerItems, SafeAreaView } from "react-navigation";

import User from "../mobx/user";

const Sidebar = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: "always", horizontal: "never" }}
    >
      <View style={styles.headerBar}>
        <View
          style={{
            marginTop: normalize(10),
            backgroundColor: "white",
            shadowOpacity: 0.5,
            shadowColor: "#333",
            shadowOffset: { height: 5, width: 5 },
            borderRadius: normalize(50)
          }}
        >
          <Image
            style={styles.img}
            source={
              User.picture !== undefined
                ? { uri: User.picture }
                : require("../assets/icon/user.png")
            }
          />
        </View>
        <Text style={styles.name}>
          {User.firstname} {User.lastname}
        </Text>
        <Text style={styles.role}>{User.role}</Text>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerBar: {
    justifyContent: "center",
    alignItems: "center"
  },
  name: {
    marginTop: normalize(5),
    marginBottom: normalize(-2.5),
    fontSize: normalize(14),
    color: "white"
  },
  role: {
    marginTop: normalize(-2.5),
    fontSize: normalize(14),
    color: color.SubMain
  },
  img: {
    height: normalize(100),
    width: normalize(100),
    borderRadius: normalize(50),
    resizeMode: "cover"
  }
});

export default Sidebar;
