import React, { Component } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  FlatList,
  Modal,
  Alert,
  Image,
  View,
  Text
} from "react-native";
import { Dropdown } from "react-native-material-dropdown";

import User from "../mobx/user";
import font from "../assets/font";
import color from "../statics/color";
import normalize from "../assets/normalize";
import { get, post } from "../services/index";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class ModalAddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      status: [],
      interest: [],
      item_product_id: null,
      item_product_name: "",
      item_product_detail: "",
      item_status: "",
      addProduct: []
    };
  }

  componentWillMount = () => {
    this._fetch();
  };

  _fetch = async () => {
    let { customer_id } = this.props;
    await get("get_product", User.token).then(res => {
      this.setState({ product: res.result });
    });
    await get("get_all_status", User.token).then(res => {
      res.result.forEach(el => {
        if (el.status === "hot") {
          el.value = "เร่งปิดการขาย";
        } else if (el.status === "warm") {
          el.value = "การขายปานกลาง";
        } else if (el.status === "cool") {
          el.value = "การขายปกติ";
        } else if (el.status === "wait") {
          el.value = "รอดำเนินการ";
        }
      });
      this.setState({ status: res.result });
    });
    let obj = { customer_id };
    await post("get_interest_by_customer", obj, User.token).then(res => {
      data = res.result;
      this.setState({ interest: res.result });
    });
    await get("customer/" + customer_id, User.token).then(res => {
      this.setState({
        customer_name: `${res.result[0].name} ${res.result[0].lastname}`
      });
    });
  };

  _addProduct = () => {
    let {
      interest,
      addProduct,
      item_product_id,
      item_product_name,
      item_product_detail,
      item_status
    } = this.state;

    function notEqual(value, index, array) {
      return value.product_id !== item_product_id;
    }

    if (
      interest.every(notEqual) &&
      addProduct.every(notEqual) &&
      item_product_id !== null &&
      item_status !== ""
    ) {
      this.setState({
        addProduct: [
          ...addProduct,
          {
            product_id: item_product_id,
            product_name: item_product_name,
            product_detail: item_product_detail,
            status_name: item_status,
            status:
              item_status === "hot"
                ? 1
                : item_status === "warm"
                ? 2
                : item_status === "cool"
                ? 3
                : item_status === "wait"
                ? 0
                : null
          }
        ],
        interest: [
          ...interest,
          {
            product_id: item_product_id,
            product_name: item_product_name,
            product_detail: item_product_detail,
            status_name: item_status,
            status:
              item_status === "hot"
                ? 1
                : item_status === "warm"
                ? 2
                : item_status === "cool"
                ? 3
                : item_status === "wait"
                ? 0
                : null,
            visible: true
          }
        ]
      });
    }
  };

  _addInterest = async () => {
    let { addProduct } = this.state;
    let obj = {
      customer_id: this.props.customer_id,
      product: addProduct
    };
    await post("create_customer_interest", obj, User.token).then(res => {
      this._fetch();
      setTimeout(() => {
        Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
      }, 200);
    });
  };

  _removeInterest = async interest_id => {
    let obj = { interest_id: interest_id };
    await post("remove_interest", obj, User.token).then(res => {
      this._fetch();
      setTimeout(() => {
        Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
      }, 200);
    });
  };

  _renderItem = item => {
    return item.status !== "close" ? (
      <View
        style={[
          styles.listContainer,
          item.visible ? { borderColor: color.SubGreen } : null
        ]}
      >
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              height: normalize(40),
              width: normalize(40)
            }}
          >
            {!item.visible ? (
              <Image
                style={{
                  height: normalize(25),
                  width: normalize(25),
                  resizeMode: "contain"
                }}
                source={require("../assets/icon_mobile/select.png")}
              />
            ) : null}
          </View>
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <Text style={styles.name}>{item.product_name}</Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Text numberOfLines={1} style={styles.description}>
                {item.product_detail}
              </Text>
              {item.status_name === "hot" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image
                    style={styles.iconStatus}
                    source={require("../assets/icon/hot_grey.png")}
                  />
                  <Text style={styles.status}>เร่งปิดการขาย</Text>
                </View>
              ) : null}
              {item.status_name === "warm" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image
                    style={styles.iconStatus}
                    source={require("../assets/icon/warm_grey.png")}
                  />
                  <Text style={styles.status}>การขายปานกลาง</Text>
                </View>
              ) : null}
              {item.status_name === "cool" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image
                    style={styles.iconStatus}
                    source={require("../assets/icon/cold_grey.png")}
                  />
                  <Text style={styles.status}>การขายปกติ</Text>
                </View>
              ) : null}
            </View>
          </View>
        </View>
        {!item.visible ? (
          <TouchableOpacity
            style={{
              alignItems: "center",
              justifyContent: "center",
              height: normalize(40),
              width: normalize(40)
            }}
            onPress={() => this._removeInterest(item.interest_id)}
          >
            <Image
              style={{
                height: normalize(20),
                width: normalize(20),
                resizeMode: "contain"
              }}
              source={require("../assets/icon_mobile/icon_delete.png")}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    ) : null;
  };

  render() {
    const { visible } = this.props;
    return (
      <Modal
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          console.log("Modal has been closed.");
        }}
      >
        <TouchableOpacity
          style={styles.backgroundModal}
          onPress={() => this.props.dismissModal()}
        />

        <View style={styles.modal}>
          <View style={styles.head}>
            <Text style={styles.headText}>เพิ่มสินค้า</Text>
          </View>
          <Text
            style={{
              borderBottomColor: "lightgrey",
              borderBottomWidth: normalize(0.5),
              marginTop: normalize(15),
              fontSize: normalize(14),
              color: color.Main
            }}
          >
            ชื่อลูกค้า : {this.state.customer_name}
          </Text>
          <View>
            <Dropdown
              label="สินค้า"
              itemCount={5}
              data={this.state.product}
              baseColor={color.Main}
              itemColor={"lightgrey"}
              fontSize={normalize(14)}
              style={{ color: color.SubMain }}
              selectedItemColor={color.SubMain}
              floatingLabelEnabled={true}
              labelFontSize={normalize(14)}
              inputContainerStyle={{
                borderBottomColor: color.SubMain,
                borderBottomWidth: 1
              }}
              containerStyle={{
                marginTop: aspectRatio < 1.6 ? normalize(10) : normalize(0)
              }}
              overlayStyle={{
                marginTop: normalize(80)
              }}
              onChangeText={(value, index, data) => {
                this.setState({
                  item_product_id: data[index].id_product,
                  item_product_name: data[index].product_name,
                  item_product_detail: data[index].product_detail
                });
              }}
            />
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Dropdown
                label="สถานะการขาย"
                itemCount={5}
                data={this.state.status}
                baseColor={color.Main}
                itemColor={"lightgrey"}
                fontSize={normalize(14)}
                style={{ color: color.SubMain }}
                selectedItemColor={color.SubMain}
                floatingLabelEnabled={true}
                labelFontSize={normalize(14)}
                inputContainerStyle={{
                  borderBottomColor: color.SubMain,
                  borderBottomWidth: 1
                }}
                containerStyle={{
                  width: width / 2.1,
                  marginTop: aspectRatio < 1.6 ? normalize(10) : normalize(0)
                }}
                overlayStyle={{
                  marginTop: normalize(80)
                }}
                onChangeText={(value, index, data) => {
                  this.setState({ item_status: data[index].status });
                }}
              />
              <TouchableOpacity
                style={styles.button}
                onPress={() => this._addProduct()}
              >
                <Text style={styles.buttonText}>เพิ่มสินค้า</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                marginTop: normalize(15),
                marginBottom: normalize(5),
                borderColor: "lightgrey",
                borderBottomWidth: normalize(0.5)
              }}
            >
              <Text
                style={{
                  borderBottomColor: "lightgrey",
                  borderBottomWidth: normalize(0.5),
                  fontSize: normalize(14),
                  color: color.Main
                }}
              >
                รายการสินค้าที่สนใจ
              </Text>
            </View>
            <FlatList
              data={this.state.interest}
              style={{ height: normalize(150) }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => this._renderItem(item)}
            />
          </View>
          <TouchableOpacity
            style={styles.buttonBlue}
            onPress={() => this._addInterest()}
          >
            <Text style={styles.buttonText}>เพิ่มสินค้า</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    height: normalize(40),
    margin: normalize(2),
    flexDirection: "row",
    borderWidth: 0.5,
    borderRadius: normalize(2),
    borderColor: "lightgrey",
    justifyContent: "space-between"
  },
  backgroundModal: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    position: "absolute",
    height: height,
    width: width,
    zIndex: 0
  },
  modal: {
    padding: normalize(15),
    width: width * 0.85,
    top: aspectRatio < 1.6 ? height * 0.15 : height * 0.125,
    left: width * 0.075,
    backgroundColor: "white",
    justifyContent: "center",
    position: "absolute",
    borderRadius: normalize(3),
    zIndex: 1
  },
  head: {
    height: normalize(25),
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  headText: {
    fontSize: normalize(16),
    color: color.Main,
    fontFamily: font.bold
  },
  name: {
    marginVertical: normalize(-2),
    fontSize: normalize(12),
    fontFamily: font.bold,
    color: color.Main
  },
  description: {
    width: width * 0.25,
    marginVertical: normalize(-2),
    fontSize: normalize(10),
    color: color.SubMain
  },
  iconStatus: {
    marginBottom: normalize(-5),
    marginRight: normalize(5),
    height: normalize(10),
    width: normalize(10),
    resizeMode: "contain"
  },
  status: {
    marginVertical: normalize(-2),
    fontSize: normalize(10),
    color: "lightgrey"
  },
  button: {
    width: width / 3.5,
    marginTop: normalize(10),
    backgroundColor: color.SubGreen,
    borderRadius: normalize(40),
    alignItems: "center",
    alignSelf: "center",
    padding: normalize(10)
  },
  buttonBlue: {
    width: width * 0.85,
    bottom: normalize(-15),
    borderBottomRightRadius: normalize(3),
    borderBottomLeftRadius: normalize(3),
    backgroundColor: color.Main,
    marginTop: normalize(10),
    padding: normalize(15),
    alignItems: "center",
    alignSelf: "center"
  },
  buttonText: {
    fontSize: normalize(14),
    color: "white"
  }
});
