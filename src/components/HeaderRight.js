import React from "react";
import { Platform, Image, View } from "react-native";

import User from "../mobx/user";
import normalize from "../assets/normalize.js";

export default class HeaderRight extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          marginRight: normalize(10),
          backgroundColor: "white",
          alignItems: "center",
          justifyContent: "center",
          shadowOpacity: 0.5,
          shadowColor: "lightgrey",
          shadowOffset: { height: 5, width: 5 },
          borderRadius: Platform.OS === "ios" ? normalize(22.5) : normalize(135)
        }}
      >
        <Image
          source={
            User.picture !== undefined
              ? { uri: User.picture }
              : require("../assets/icon/user.png")
          }
          resizeMode="cover"
          style={{
            height: normalize(45),
            width: normalize(45),
            borderRadius:
              Platform.OS === "ios" ? normalize(22.5) : normalize(135)
          }}
        />
      </View>
    );
  }
}
