import React, { Component } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  FlatList,
  Modal,
  Alert,
  Image,
  View,
  Text
} from "react-native";
import { FormLabel, FormInput } from "react-native-elements";

import User from "../mobx/user";
import font from "../assets/font";
import color from "../statics/color";
import normalize from "../assets/normalize";
import { get, post } from "../services/index";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class ModalEditCategory extends Component {
  dismissModal;

  constructor(props) {
    super(props);
    this.state = {
      category: [],
      type: ""
    };
  }

  componentWillMount = () => {
    this._fetch();
  };

  _fetch = async () => {
    await get("get_product_type", User.token).then(res => {
      this.setState({
        category: res.result
      });
    });
  };

  _createType = async () => {
    if (this.state.type) {
      let obj = { type: this.state.type };
      await post("create_product_type", obj, User.token).then(res => {
        Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
      });
    } else {
      Alert.alert("ผิดพลาด", "กรุณากรอกชื่อประเภทสินค้า");
    }
  };

  _deleteType = async id => {
    let obj = {
      id_product_type: id
    };
    await post("delete_product_type", obj, User.token).then(res => {
      Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
    });
  };

  _renderItem = ({ item }) => {
    return (
      <View style={styles.listContainer}>
        <View style={{ justifyContent: "center" }}>
          <Text
            style={{
              fontSize: normalize(14),
              color: color.Main,
              justifyContent: "center"
            }}
          >
            {item.value}
          </Text>
        </View>
        <TouchableOpacity
          style={{ justifyContent: "center" }}
          onPress={() => {
            this._deleteType(item.id).then(() => this._fetch());
          }}
        >
          <Image
            style={{
              height: normalize(20),
              width: normalize(20),
              resizeMode: "contain"
            }}
            source={require("../assets/icon_mobile/icon_delete.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { visible } = this.props;
    return (
      <Modal
        transparent={true}
        visible={visible}
        onRequestClose={() => {
          console.log("Modal has been closed.");
        }}
      >
        <TouchableOpacity
          style={styles.backgroundModal}
          onPress={() => this.props.dismissModal()}
        />

        <View style={styles.modal}>
          <View style={styles.head}>
            <Text style={styles.headText}>แก้ไขประเภทสินค้า</Text>
          </View>
          <View>
            <FlatList
              style={{
                maxHeight: normalize(182),
                borderColor: "lightgrey",
                borderTopWidth: normalize(0.5),
                borderBottomWidth: normalize(0.5)
              }}
              data={this.state.category}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this._renderItem}
            />
          </View>

          <View
            style={{
              marginTop: normalize(15),
              marginBottom: normalize(5),
              borderColor: "lightgrey",
              borderBottomWidth: normalize(0.5)
            }}
          >
            <Text
              style={{
                borderBottomColor: "lightgrey",
                borderBottomWidth: normalize(0.5),
                fontSize: normalize(14),
                color: color.Main
              }}
            >
              เพิ่มประเภทสินค้าใหม่
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View>
              <FormLabel labelStyle={styles.label}>ชื่อประเภทสินค้า</FormLabel>
              <FormInput
                containerStyle={{
                  marginLeft: normalize(10),
                  marginRight: normalize(10),
                  marginVertical:
                    aspectRatio < 1.6 ? normalize(5) : normalize(-5),
                  borderBottomColor: color.SubMain
                }}
                inputStyle={styles.input}
                placeholder="กรอกชื่อสินค้า"
                underlineColorAndroid={color.SubMain}
                onChangeText={res => {
                  this.setState({
                    type: res
                  });
                }}
              />
            </View>

            <TouchableOpacity
              style={styles.buttonBlue}
              onPress={() => {
                this._createType().then(() => this._fetch());
              }}
            >
              <Text style={styles.buttonText}>เพิ่ม</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    height: normalize(40),
    marginVertical: normalize(2.5),
    marginHorizontal: normalize(10),
    paddingVertical: normalize(5),
    paddingHorizontal: normalize(20),
    flexDirection: "row",
    borderWidth: 0.5,
    borderRadius: normalize(2),
    borderColor: "lightgrey",
    justifyContent: "space-between"
  },
  backgroundModal: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    position: "absolute",
    height: height,
    width: width,
    zIndex: 0
  },
  modal: {
    padding: normalize(15),
    width: width * 0.85,
    top: aspectRatio < 1.6 ? height * 0.2 : height * 0.175,
    left: width * 0.075,
    backgroundColor: "white",
    justifyContent: "center",
    position: "absolute",
    borderRadius: normalize(3),
    zIndex: 1
  },
  head: {
    height: normalize(25),
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  headText: {
    fontSize: normalize(16),
    color: color.Main,
    fontFamily: font.bold
  },
  buttonBlue: {
    borderRadius: normalize(30),
    backgroundColor: color.Main,
    marginTop: normalize(10),
    padding: normalize(15),
    alignItems: "center",
    alignSelf: "center"
  },
  buttonText: {
    fontSize: normalize(14),
    color: "white"
  },
  label: {
    marginTop: normalize(5),
    marginLeft: normalize(10),
    marginRight: normalize(10),
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    width: aspectRatio < 1.6 ? width * 0.65 : width * 0.55,
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  }
});
