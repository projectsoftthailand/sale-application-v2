import React from "react";
import { TouchableOpacity, Image } from "react-native";
import normalize from "../assets/normalize.js";
import Burger from "../assets/icon_mobile/menu.png";

export default class BurgerMenu extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity
        style={{
          width: "100%",
          height: "100%",
          alignItems: "center",
          justifyContent: "center"
        }}
        onPress={() => this.props.navigation.openDrawer()}
      >
        <Image
          source={Burger}
          style={{
            margin: normalize(15),
            height: normalize(25),
            width: normalize(25),
            resizeMode: "contain"
          }}
        />
      </TouchableOpacity>
    );
  }
}
