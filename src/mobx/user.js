import { observable, action } from "mobx";
import { AsyncStorage } from "react-native";
import { url } from "../services/index";

class User {
  @observable
  user_id = null;
  @observable
  username = "";
  @observable
  role = "";
  @observable
  firstname = "";
  @observable
  lastname = "";
  @observable
  picture = "";
  @observable
  phone_num = "";
  @observable
  token = "";
  async = null;
  constructor() {
    this.initial();
  }

  @action
  initial = () => {
    this.async = AsyncStorage.getItem("User").then(user => {
      if (user) {
        let res = JSON.parse(user);
        this.auth = true;
        this.username = res.username;
        this.user_id = res.user_id;
        this.role = res.role;
        this.firstname = res.firstname;
        this.lastname = res.lastname;
        this.picture = res.picture;
        this.phone_num = res.phone_num;
        this.position = res.position;
        this.token = res.token;
      }
    });
  };

  @action
  login(user) {
    this.username = user.username;
    this.user_id = user.user_id;
    this.role = user.role;
    this.firstname = user.name;
    this.lastname = user.lastname;
    this.picture = url + "user/profile/" + user.picture;
    this.phone_num = user.phone_num;
    this.position = user.position;
    this.token = user.token;

    let obj = {
      username: user.username,
      user_id: user.user_id,
      role: user.role,
      firstname: user.name,
      lastname: user.lastname,
      picture: url + "user/profile/" + user.picture,
      phone_num: user.phone_num,
      position: user.position,
      token: user.token
    };
    AsyncStorage.setItem("User", JSON.stringify(obj));
  }

  @action
  logout() {
    this.auth = false;
    AsyncStorage.removeItem("User");
  }

  @action
  changeUserData(base64, firstname, lastname, position, phone_num) {
    this.picture = base64;
    this.firstname = firstname;
    this.lastname = lastname;
    this.position = position;
    this.phone_num = phone_num;

    let obj = {
      picture: base64,
      firstname: firstname,
      lastname: lastname,
      position: position,
      phone_num: phone_num
    };
    AsyncStorage.mergeItem("User", JSON.stringify(obj));
  }
}

export default new User();
