import React from "react";
import { StyleSheet, Dimensions, Image, View, Text } from "react-native";
import { createDrawerNavigator, createStackNavigator } from "react-navigation";

import color from "../statics/color";
import font from "../assets/font";

import Sidebar from "../components/Sidebar";
import AddCustomer from "../components/AddCustomer";
import BurgerMenu from "../components/BurgerMenu";
import HeaderRight from "../components/HeaderRight";
import HeaderBackButton from "../components/HeaderBackButton";

import Dashboard from "../screen/Dashboard";
import EditScreen from "../screen/EditScreen";
import ReportScreen from "../screen/ReportScreen";
import LogoutScreen from "../screen/LogoutScreen";

import CloseScreen from "../screen/Sale/CloseScreen";
import InterrestedScreen from "../screen/Sale/InterrestedScreen";

import CareScreen from "../screen/Sale/Care/CareScreen";
import CareAddScreen from "../screen/Sale/Care/CareAddScreen";
import CareContactScreen from "../screen/Sale/Care/CareContactScreen";

import ProductScreen from "../screen/Product/ProductScreen";
import ProductEditScreen from "../screen/Product/ProductEditScreen";
import ProductDetailScreen from "../screen/Product/ProductDetailScreen";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

const Main = createStackNavigator({
  Main: {
    screen: Dashboard,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BurgerMenu navigation={navigation} />,
      headerTitle: (
        <Image
          style={styles.logo}
          source={require("../assets/image/logo_nav.png")}
        />
      ),
      headerRight: <HeaderRight />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  },
  ProductDetail: {
    screen: ProductDetailScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderBackButton navigation={navigation} />,
      headerTitle: <Text style={styles.header}>สินค้า</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  },
  ProductEdit: {
    screen: ProductEditScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderBackButton navigation={navigation} />,
      headerTitle: <Text style={styles.header}>แก้ไขสินค้า</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  }
});

const Care = createStackNavigator(
  {
    Care: {
      screen: CareScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BurgerMenu navigation={navigation} />,
        headerTitle: <Text style={styles.header}>ดูแลการขาย</Text>,
        headerRight: <AddCustomer navigation={navigation} />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    },
    CareContact: {
      screen: CareContactScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <HeaderBackButton navigation={navigation} />,
        headerTitle: <Text style={styles.header}>ดูแลการขาย</Text>,
        headerRight: <View />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    },
    CareAdd: {
      screen: CareAddScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <HeaderBackButton navigation={navigation} />,
        headerTitle: <Text style={styles.header}>ดูแลการขาย</Text>,
        headerRight: <View />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    }
  },
  {
    initialRouteName: "Care"
  }
);

const Close = createStackNavigator({
  Close: {
    screen: CloseScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BurgerMenu navigation={navigation} />,
      headerTitle: <Text style={styles.header}>ปิดการขาย</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  },
  CareContact: {
    screen: CareContactScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderBackButton navigation={navigation} />,
      headerTitle: <Text style={styles.header}>ดูแลการขาย</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  }
});

const Interrested = createStackNavigator({
  Interrested: {
    screen: InterrestedScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BurgerMenu navigation={navigation} />,
      headerTitle: <Text style={styles.header}>ลูกค้าที่สนใจ</Text>,
      headerRight: <AddCustomer navigation={navigation} />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  },
  CareAdd: {
    screen: CareAddScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderBackButton navigation={navigation} />,
      headerTitle: <Text style={styles.header}>ดูแลการขาย</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  }
});

const Product = createStackNavigator(
  {
    Product: {
      screen: ProductScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <BurgerMenu navigation={navigation} />,
        headerTitle: <Text style={styles.header}>สินค้า</Text>,
        headerRight: <View />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    },
    ProductDetail: {
      screen: ProductDetailScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <HeaderBackButton navigation={navigation} />,
        headerTitle: <Text style={styles.header}>สินค้า</Text>,
        headerRight: <View />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    },
    ProductEdit: {
      screen: ProductEditScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <HeaderBackButton navigation={navigation} />,
        headerTitle: <Text style={styles.header}>แก้ไขสินค้า</Text>,
        headerRight: <View />,
        headerStyle: {
          backgroundColor: "white",
          height: normalize(60)
        }
      })
    }
  },
  {
    initialRouteName: "Product"
  }
);

const Report = createStackNavigator({
  Report: {
    screen: ReportScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BurgerMenu navigation={navigation} />,
      headerTitle: <Text style={styles.header}>รายงานผล</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  }
});

const Edit = createStackNavigator({
  Edit: {
    screen: EditScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <BurgerMenu navigation={navigation} />,
      headerTitle: <Text style={styles.header}>แก้ไขข้อมูลส่วนตัว</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  }
});

const SaleDrawer = createDrawerNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "หน้าหลัก",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_home_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Care: {
      screen: Care,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "ดูแลการขาย",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_care_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Close: {
      screen: Close,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "ปิดการขาย",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_close_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Interrested: {
      screen: Interrested,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "ลูกค้าที่สนใจ",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_interrested_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Product: {
      screen: Product,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "สินค้า",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_product_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Report: {
      screen: Report,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "รายงานผล",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_summary_color.png")}
            style={styles.icon}
          />
        )
      })
    },
    Edit: {
      screen: Edit,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "แก้ไขข้อมูลส่วนตัว",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_editprofile.png")}
            style={styles.icon}
          />
        )
      })
    },
    Logout: {
      screen: LogoutScreen,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: "ออกจากระบบ",
        drawerIcon: (
          <Image
            source={require("../assets/icon_mobile/icon_logout.png")}
            style={styles.icon}
          />
        )
      })
    }
  },
  {
    initialRouteName: "Main",
    headerMode: "none",
    drawerBackgroundColor: color.Main,
    drawerWidth: aspectRatio < 1.6 ? width * 0.6 : width * 0.75,
    contentComponent: props => <Sidebar {...props} />,
    contentOptions: {
      activeBackgroundColor: color.SubMain,
      activeTintColor: "white",
      inactiveTintColor: "white",
      labelStyle: {
        fontWeight: null,
        margin: normalize(10),
        fontSize: normalize(14),
        fontFamily: font.bold
      },
      iconContainerStyle: {
        opacity: 1,
        marginLeft: normalize(20)
      }
    }
  }
);

const styles = StyleSheet.create({
  logo: {
    flex: 1,
    width: width / 2,
    height: normalize(45),
    resizeMode: "contain"
  },
  header: {
    position: "absolute",
    left: 0,
    right: 0,
    fontSize: normalize(16),
    fontFamily: font.bold,
    color: color.Main,
    fontWeight: "400",
    textAlign: "center"
  },
  icon: {
    width: normalize(24),
    height: normalize(24),
    resizeMode: "contain"
  }
});

export default SaleDrawer;
