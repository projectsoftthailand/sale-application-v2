export const ip = 'https://salemanageone.com'
// const ip = "http://192.168.1.108:3012";
export const url = ip + "/api/v1/";

export const get = (path, token) => {
  return new Promise((resolve, reject) => {
    fetch(url + path, {
      method: "GET",
      headers: {
        Authorization: token,
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(resJson => resolve(resJson))
      .catch(err => reject(err));
  });
};

export const post = (path, object, token) => {
  return new Promise((resolve, reject) => {
    fetch(url + path, {
      method: "POST",
      headers: {
        Authorization: token,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(object)
    })
      .then(res => res.json())
      .then(resJson => resolve(resJson))
      .catch(err => reject(err));
  });
};

export const postForm = (path, object, token) => {
  return new Promise((resolve, reject) => {
    fetch(url + path, {
      method: "POST",
      headers: {
        Authorization: token,
        Accept: "application/json",
        "Content-Type": "multipart/form-data"
      },
      body: object
    })
      .then(res => res.json())
      .then(resJson => resolve(resJson))
      .catch(err => reject(err));
  });
};
