export default {
  bold: "SUKHUMVITSET-BOLD",
  light: "SUKHUMVITSET-LIGHT_0",
  medium: "SUKHUMVITSET-MEDIUM",
  semibold: "SUKHUMVITSET-SEMIBOLD",
  thin: "SUKHUMVITSET-THIN_0"
};
