import { Dimensions } from "react-native";
var { height, width } = Dimensions.get("window");

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => (width / guidelineBaseWidth) * size;
const verticalScale = size => (height / guidelineBaseHeight) * size;
const fonts = (size, factor = 0.5) => size + (scale(size) - size) * factor;

// export { scale, verticalScale, normalize };

export default (normalize = size => {
  // let font = ((parseInt(size) * width) - ((1.8 - 0.002) * width)) / 400;
  return fonts(size);
});
