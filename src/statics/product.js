export default (product = [
  {
    id: 0,
    rank: 1,
    name: "EPAM",
    status: "hot",
    description: "ระบบจัดการการประชุม",
    img: require("../assets/image/product1.png")
  },
  {
    id: 1,
    rank: 2,
    name: "SMART APARTMENT",
    status: "warm",
    description: "ระบบจัดการหอพัก",
    img: require("../assets/image/product2.png")
  },
  {
    id: 2,
    rank: 3,
    name: "SALE MANAGEMENT",
    status: "cold",
    description: "ระบบจัดการขาย",
    img: require("../assets/image/product3.png")
  },
  {
    id: 3,
    name: "เว็ปไซต์",
    status: "cold",
    description: "ออกแบบเว็ปไซต์",
    img: require("../assets/image/product4.png")
  },
  {
    id: 4,
    name: "แอปพลิเคชั่น",
    status: "hot",
    description: "ออกแบบแอปพลิเคชั่น",
    img: require("../assets/image/product5.png")
  },
  {
    id: 5,
    name: "ออกแบบโลโก้",
    status: "close",
    description: "ออกแบบออกแบบโลโก้",
    img: require("../assets/image/product6.png")
  }
]);
