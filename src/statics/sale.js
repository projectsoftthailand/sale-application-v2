export default (sale = [
  {
    id: 0,
    firstname: "หมิวรา",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา ศรีโปร"
  },
  {
    id: 1,
    firstname: "ติ๊ก",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "ติ๊ก ศรีโปร"
  },
  {
    id: 2,
    firstname: "อนิส",
    lastname: "ศรีโปร",
    role: "นักพัฒนา",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "อนิส ศรีโปร"
  },
  {
    id: 3,
    firstname: "ผึ้งโม๊ะ",
    lastname: "โปรเจ็ค",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "ผึ้งโม๊ะ โปรเจ็ค"
  },
  {
    id: 4,
    firstname: "รินโน๊ะ",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "รินโน๊ะ ศรีโปร"
  },
  {
    id: 5,
    firstname: "หมิวรา",
    lastname: "ศรีโปร",
    role: "นักพัฒนา",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา ศรีโปร"
  },
  {
    id: 6,
    firstname: "หมิวรา",
    lastname: "โปรเจ็ค",
    role: "นักพัฒนา",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา โปรเจ็ค"
  },
  {
    id: 7,
    firstname: "หมิวรา",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา ศรีโปร"
  },
  {
    id: 8,
    firstname: "หมิวรา",
    lastname: "โปรเจ็ค",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา โปรเจ็ค"
  },
  {
    id: 9,
    firstname: "หมิวรา",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา ศรีโปร"
  },
  {
    id: 10,
    firstname: "หมิวรา",
    lastname: "โปรเจ็ค",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา โปรเจ็ค"
  },
  {
    id: 11,
    firstname: "หมิวรา",
    lastname: "ศรีโปร",
    role: "พนักงานขาย",
    phone: "089-9999999",
    img: require("../assets/image/sale.jpg"),
    value: "หมิวรา ศรีโปร"
  }
]);
