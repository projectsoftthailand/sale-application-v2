export default (color = {
  Main: "#035ca7",
  SubMain: "#579adc",
  MinMain: "#55b2ff",
  Red: "#ff6666",
  Orange: "#ffc269",
  SubGreen: "#b4d669",
  Green: "#7ac543"
});
