import { Dimensions } from "react-native";

import color from "./color";
import font from "../assets/font";
import normalize from "../assets/normalize";

const { width } = Dimensions.get("window");

export const conf = data => ({
  title: null,
  chart: {
    type: "column",
    scrollablePlotArea: {
      minWidth: width * 2
    }
  },
  colors: [color.SubGreen, color.Red],
  xAxis: {
    labels: {
      style: {
        fontSize: normalize(12),
        fontFamily: font.medium,
        fontWeight: "bold",
        color: color.SubMain
      }
    },
    lineColor: "lightgrey",
    tickColor: "transparent",
    categories: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."]
  },
  yAxis: {
    tickInterval: 4,
    title: {
      text: "จำนวนการขาย",
      style: {
        fontSize: normalize(12),
        fontFamily: font.medium,
        color: color.SubMain
      }
    },
    alignTicks: true,
    tickColor: "red",
    labels: {
      style: {
        fontSize: normalize(12),
        fontFamily: font.medium,
        fontWeight: "bold",
        color: color.SubMain
      }
    },
    lineWidth: 1,
    tickWidth: 1,
    lineColor: "lightgrey",
    tickColor: "lightgrey"
  },
  legend: {
    itemStyle: {
      color: "grey",
      fontSize: normalize(12),
      fontFamily: font.medium
    },
    margin: normalize(-15),
    padding: normalize(15),
    verticalAlign: "top"
  },
  exporting: {
    enabled: false
  },
  credits: {
    enabled: false
  },
  series: [
    {
      name: "ปิดการขายแล้ว",
      data: data.close
    },
    {
      name: "ยังไม่ปิดการขาย",
      data: data.unclose
    }
  ]
});
