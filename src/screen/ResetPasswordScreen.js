import React, { Component } from "react";
import { FormLabel, FormInput } from "react-native-elements";
import { TouchableOpacity, StyleSheet, Dimensions, AppRegistry, View, Text, Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { post } from "../services/index";
import normalize from "../assets/normalize";
import color from "../statics/color";
import font from "../assets/font";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class ResetPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check: true,
      user_id: null,
      username: null,
      name: null,
      last_name: null,
      password: null,
      verify_password: null
    };
  }

  checkData = async () => {
    let { username, name, last_name } = this.state;
    let obj = { username, name, last_name };
    try {
      await post("user/check_reset_password", obj, null).then(res => {
        if (res.success) {
          this.setState({
            check: false,
            user_id: res.result.user_id
          });
          Alert.alert("สำเร็จ", res.message);
        } else {
          Alert.alert("ผิดพลาด", res.message);
        }
      });
    } catch (err) {
      alert(err);
    }
  };

  resetPassword = async () => {
    let { password, verify_password, user_id } = this.state;
    let obj = { user_id, password };
    if (password === verify_password && password && verify_password) {
      try {
        await post("user/reset_password", obj, null).then(res => {
          if (res.success) {
            Alert.alert("สำเร็จ", res.message);
            this.props.navigation.goBack();
          } else {
            Alert.alert("ผิดพลาด", res.message);
          }
        });
      } catch (err) {
        alert(err);
      }
    } else {
      Alert.alert("ผิดพลาด", "กรุณากรอกข้อมูลให้ครบถ้วน");
    }
  };

  render() {
    return this.state.check ? (
      <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: "white" }}>
        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>username</FormLabel>
          <FormInput
            containerStyle={styles.containerStyle}
            inputStyle={styles.input}
            placeholder={"กรอก username"}
            underlineColorAndroid={color.SubMain}
            autoCapitalize="none"
            onChangeText={res => this.setState({ username: res })}
          />
          <FormLabel labelStyle={styles.label}>ชื่อจริง</FormLabel>
          <FormInput
            containerStyle={styles.containerStyle}
            inputStyle={styles.input}
            placeholder={"กรอกชื่อจริง"}
            underlineColorAndroid={color.SubMain}
            autoCapitalize="none"
            onChangeText={res => this.setState({ name: res })}
          />
          <FormLabel labelStyle={styles.label}>นามสกุล</FormLabel>
          <FormInput
            containerStyle={styles.containerStyle}
            inputStyle={styles.input}
            placeholder={"กรอกนามสกุล"}
            underlineColorAndroid={color.SubMain}
            autoCapitalize="none"
            onChangeText={res => this.setState({ last_name: res })}
          />
        </View>

        <TouchableOpacity style={[styles.button]} onPress={() => this.checkData()}>
          <Text style={{ fontSize: normalize(14), color: "white" }}>ยืนยัน</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    ) : (
      <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: "white" }}>
        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>รหัสผ่านใหม่</FormLabel>
          <FormInput
            autoCapitalize="none"
            placeholder="รหัสผ่านใหม่"
            inputStyle={styles.input}
            containerStyle={styles.containerStyle}
            underlineColorAndroid={color.SubMain}
            secureTextEntry={true}
            onChangeText={res => this.setState({ password: res })}
          />
          <FormLabel labelStyle={styles.label}>ยืนยันรหัส</FormLabel>
          <FormInput
            autoCapitalize="none"
            placeholder="ยืนยันรหัส"
            inputStyle={styles.input}
            containerStyle={styles.containerStyle}
            underlineColorAndroid={color.SubMain}
            secureTextEntry={true}
            onChangeText={res => this.setState({ verify_password: res })}
          />
        </View>

        <TouchableOpacity style={styles.button} onPress={() => this.resetPassword()}>
          <Text style={{ fontSize: normalize(14), color: "white" }}>ยืนยัน</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerInput: {
    
    marginVertical: aspectRatio < 1.6 ? normalize(5) : normalize(-10),
    borderBottomColor: color.SubMain
  },
  inputView: {
    // display: "flex",
    // alignSelf: "center",
    ...(aspectRatio < 1.6
      ? {
          paddingTop: normalize(10),
          paddingBottom: normalize(20)
        }
      : {
          paddingTop: normalize(5),
          paddingBottom: normalize(20)
        })
  },
  button: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: color.Main,
    padding: normalize(10),
    width: width * 0.6,
    borderRadius: normalize(50),
    marginTop: normalize(5)
  },
  label: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  }
});

AppRegistry.registerComponent("ResetPasswordScreen", () => ResetPasswordScreen);
