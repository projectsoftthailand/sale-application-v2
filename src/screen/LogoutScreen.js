import React, { Component } from "react";
import { NavigationActions, StackActions } from "react-navigation";
import { View, AppRegistry } from "react-native";

import User from "../mobx/user";
import { get } from "../services/index";

const resetAction = StackActions.reset({
  index: 0,
  key: null,
  actions: [NavigationActions.navigate({ routeName: "Login" })]
});

export default class LogoutScreen extends Component {
  componentWillMount = async () => {
    User.logout();
    await get("user/logout", null);
    this.props.navigation.dispatch(resetAction);
  };

  render() {
    return <View />;
  }
}

AppRegistry.registerComponent("LogoutScreen", () => LogoutScreen);
