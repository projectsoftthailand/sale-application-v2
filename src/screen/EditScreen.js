import React, { Component } from "react";
import { TouchableOpacity, AppRegistry, StyleSheet, Dimensions, Image, Alert, View, Text } from "react-native";
import ImagePicker from "react-native-image-picker";
import LinearGradient from "react-native-linear-gradient";
import { FormLabel, FormInput } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { postForm } from "../services/index";
import normalize from "../assets/normalize";
import color from "../statics/color";
import font from "../assets/font";
import User from "../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class EditScreen extends Component {
  constructor() {
    super();
    this.state = {
      filePic: null,
      imageSource: User.picture,
      firstname: User.firstname,
      lastname: User.lastname,
      position: User.position,
      phone: User.phone_num
    };
  }

  AddPhoto = () => {
    const options = {
      title: "Select Avatar",
      quality: 1.0,
      maxWidth: 256,
      maxHeight: 256,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        console.log("User selected a file form camera or gallery", response);
        this.setState({
          filePic: response,
          imageSource: "data:image/jpeg;base64," + response.data
        });
      }
    });
  };

  _editProfile = async () => {
    let data = new FormData();
    let { filePic, firstname, lastname, position, phone, imageSource } = this.state;
    filePic
      ? data.append("fileData", {
          uri: filePic.uri,
          type: "image/jpg",
          name: filePic.fileName
        })
      : null;
    data.append("name", firstname);
    data.append("last_name", lastname);
    data.append("position", position);
    data.append("phone", phone);
    User.changeUserData(imageSource, firstname, lastname, position, phone);
    try {
      await postForm("user/edit_user", data, User.token).then(res => {
        if (res.success) {
          this.props.navigation.navigate("Main");
          Alert.alert("สำเร็จ", res.message);
        } else {
          Alert.alert("ผิดพลาด", res.error_message ? res.error_message : res.message);
        }
      });
    } catch (err) {
      alert(err);
    }
  };

  _checkData = () => {
    !this.state.firstname || !this.state.lastname || !this.state.position || !this.state.phone
      ? Alert.alert("ผิดพลาด", "กรุณากรอกข้อมูลให้ครบถ้วน")
      : isNaN(this.state.phone)
      ? Alert.alert("ผิดพลาด", "กรุณากรอกเบอร์โทรศัพท์เป็นตัวเลข")
      : this._editProfile();
  };

  render() {
    return (
      <KeyboardAwareScrollView style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <View style={styles.headContainer}>
          <View style={styles.imgView}>
            <Image
              style={styles.img}
              source={this.state.imageSource ? { uri: this.state.imageSource } : require("../assets/icon/user.png")}
            />
          </View>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={[color.Main, color.SubMain]}
          style={{ height: normalize(80) }}
        />
        <View style={{ height: normalize(80) }} />
        <TouchableOpacity style={styles.button} onPress={this.AddPhoto}>
          <Image style={styles.icon} source={require("../assets/icon/icon_picture_white.png")} />
          <Text style={{ fontSize: normalize(14), color: "white" }}>เลือกโปรไฟล์</Text>
        </TouchableOpacity>

        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>ชื่อจริง</FormLabel>
          <FormInput
            value={this.state.firstname}
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder={"กรอกชื่อจริง"}
            underlineColorAndroid={color.SubMain}
            onChangeText={res => this.setState({ firstname: res })}
          />
          <FormLabel labelStyle={styles.label}>นามสกุล</FormLabel>
          <FormInput
            value={this.state.lastname}
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder={"กรอกนามสกุล"}
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                lastname: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>ตำแหน่ง</FormLabel>
          <FormInput
            value={this.state.position}
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder={"กรอกตำแหน่ง"}
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                position: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>หมายเลขโทรศัพท์</FormLabel>
          <FormInput
            value={this.state.phone}
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder={"กรอกหมายเลขโทรศัพท์"}
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                phone: res
              });
            }}
          />
        </View>
        <TouchableOpacity style={[styles.button, { width: width * 0.6 }]} onPress={() => this._checkData()}>
          <Text style={{ fontSize: normalize(14), color: "white" }}>บันทึก</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headContainer: {
    height: normalize(160),
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    position: "absolute",
    width: width,
    zIndex: 99
  },
  containerInput: {
    marginVertical: aspectRatio < 1.6 ? normalize(5) : normalize(-10),
    borderBottomColor: color.SubMain
  },
  imgView: {
    shadowOpacity: 0.5,
    shadowColor: "lightgrey",
    shadowOffset: { height: 5, width: 5 }
  },
  img: {
    backgroundColor: "white",
    width: normalize(130),
    height: normalize(130),
    borderRadius: normalize(65),
    borderWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  icon: {
    marginRight: normalize(10),
    width: normalize(20),
    height: normalize(20),
    resizeMode: "contain"
  },
  button: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: color.Main,
    padding: normalize(10),
    width: width * 0.4,
    borderRadius: normalize(50)
  },
  inputView: {
    // display: "flex",
    // alignSelf: "center",
    ...(aspectRatio < 1.6
      ? {
          paddingTop: normalize(10),
          paddingBottom: normalize(20)
        }
      : {
          paddingTop: normalize(5),
          paddingBottom: normalize(20)
        })
  },
  label: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  }
});

AppRegistry.registerComponent("EditScreen", () => EditScreen);
