import React, { Component } from "react";
import { View, AppRegistry } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";

const resetAction = StackActions.reset({
  index: 0,
  key: null,
  actions: [NavigationActions.navigate({ routeName: "Login" })]
});

export default class SplashScreen extends Component {
  componentDidMount = () => {
    setTimeout(() => {
      this.props.navigation.dispatch(resetAction);
    }, 1000);
  };

  render() {
    return <View />;
  }
}

AppRegistry.registerComponent("SplashScreen", () => SplashScreen);
