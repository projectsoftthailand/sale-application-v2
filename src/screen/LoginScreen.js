import React from "react";
import {
  View,
  Text,
  Image,
  Alert,
  TextInput,
  Dimensions,
  StyleSheet,
  AppRegistry,
  TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { NavigationActions, StackActions } from "react-navigation";

import User from "../mobx/user";
import font from "../assets/font";
import color from "../statics/color";
import { post } from "../services/index";
import normalize from "../assets/normalize";

import logo from "../assets/image/logo.png";
import icon_user from "../assets/icon/icon_user.png";
import icon_password from "../assets/icon/icon_password.png";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

const resetAction = role =>
  StackActions.reset({
    index: 0,
    key: null,
    actions: [NavigationActions.navigate({ routeName: role })]
  });

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  componentWillMount = () => {
    if (User.auth) {
      User.role === "admin"
        ? this.props.navigation.dispatch(resetAction("Admin"))
        : this.props.navigation.dispatch(resetAction("Sale"));
    }
  };

  CheckLogin = async () => {
    let obj = {
      username: this.state.username,
      password: this.state.password
    };
    try {
      await post("user/login", obj, null).then(res => {
        if (res.success) {
          User.login(res);
          res.role === "admin"
            ? this.props.navigation.dispatch(resetAction("Admin"))
            : this.props.navigation.dispatch(resetAction("Sale"));
        } else {
          Alert.alert("ผิดพลาด", res.error_message);
        }
      });
    } catch (err) {
      Alert.alert("ผิดพลาด", err);
    }
  };

  goResetPassword = () => {
    this.props.navigation.navigate("ResetPassword");
  };

  render() {
    return (
      <KeyboardAwareScrollView style={styles.background}>
        <View style={styles.container}>
          <Image style={styles.mainLogo} source={logo} />
          <View style={styles.inputContainer}>
            <View style={styles.inputIcon}>
              <Image style={styles.iconUser} source={icon_user} />
            </View>
            <View style={styles.TextInputContainer}>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                value={this.state.username}
                placeholderTextColor={color.SubMain}
                onChangeText={username => this.setState({ username })}
                placeholder="ชื่อผู้ใช้"
              />
            </View>
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputIcon}>
              <Image style={styles.iconPassword} source={icon_password} />
            </View>
            <View style={styles.TextInputContainer}>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                value={this.state.password}
                placeholderTextColor={color.SubMain}
                onChangeText={password => this.setState({ password })}
                placeholder="รหัสผ่าน"
                secureTextEntry={true}
              />
            </View>
          </View>

          <View style={{ width: width * 0.75 }}>
            <TouchableOpacity style={styles.forget} onPress={() => this.goResetPassword()}>
              <Text style={{ fontFamily: font.medium, color: "white", fontSize: normalize(12) }}>ลืมรหัสผ่าน?</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.state.username && this.state.password ? this.CheckLogin() : alert("กรุณากรอกข้อมูลให้ครบถ้วน");
            }}
          >
            <Text style={{ fontSize: normalize(16), color: "white" }}>เข้าสู่ระบบ</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: color.Main
  },
  container: {
    flex: 1,
    marginTop: height / 8,
    justifyContent: "center",
    alignItems: "center"
  },
  mainLogo: {
    justifyContent: "center",
    alignItems: "center",
    resizeMode: "contain",
    marginBottom: normalize(30),
    height: aspectRatio < 1.6 ? width / 3 : width / 2
  },
  inputContainer: {
    flexDirection: "row",
    marginVertical: normalize(10),
    width: width,
    height: normalize(50),
    justifyContent: "center"
  },
  inputIcon: {
    backgroundColor: color.SubMain,
    justifyContent: "center",
    alignItems: "center",
    width: width * 0.15,
    height: normalize(50),
    borderTopLeftRadius: normalize(5),
    borderBottomLeftRadius: normalize(5),
    borderTopRightRadius: normalize(0.2),
    borderBottomRightRadius: normalize(0.2)
  },
  iconUser: {
    width: normalize(25),
    height: normalize(25),
    resizeMode: "contain"
  },
  iconPassword: {
    width: normalize(20),
    height: normalize(20),
    resizeMode: "contain"
  },
  TextInputContainer: {
    backgroundColor: "white",
    borderTopLeftRadius: normalize(0.2),
    borderBottomLeftRadius: normalize(0.2),
    borderTopRightRadius: normalize(5),
    borderBottomRightRadius: normalize(5),
    width: width * 0.65,
    height: normalize(50),
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    backgroundColor: "transparent",
    alignContent: "center",
    color: color.Main,
    width: width * 0.65,
    height: normalize(48),
    fontSize: normalize(16),
    paddingHorizontal: normalize(15),
    fontFamily: font.medium
  },
  button: {
    alignItems: "center",
    backgroundColor: color.SubMain,
    padding: normalize(10),
    borderRadius: normalize(50),
    marginTop: normalize(30),
    width: width * 0.8
  },
  forget: {
    alignItems: "flex-end"
  }
});

AppRegistry.registerComponent("LoginScreen", () => LoginScreen);
