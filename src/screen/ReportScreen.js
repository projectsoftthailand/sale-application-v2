import React, { Component } from "react";
import { ActivityIndicator, Dimensions, StyleSheet, Image, View, Text } from "react-native";
import { Dropdown } from "react-native-material-dropdown";
import ChartView from "react-native-highcharts";

import normalize from "../assets/normalize";
import { get } from "../services/index";
import { conf } from "../statics/graph";
import color from "../statics/color";
import font from "../assets/font";
import User from "../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
var month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
var sale = null;
var hot = null;
var warm = null;
var cool = null;
var close = null;
var year = [];
var dataInMonth = {};
var now = new Date().getFullYear();
export default class ReportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      selectYear: now
    };
  }

  componentWillMount = async () => {
    hot = 0;
    warm = 0;
    cool = 0;
    close = 0;
    this._fetch();
  };

  _fetch = () => {
    User.role === "admin"
      ? this._get_admin_data().then(() => this.setState({ loading: false }))
      : this._get_sale_data().then(() => this.setState({ loading: false }));
  };

  _get_admin_data = async () => {
    let resClose = {};
    let resUnclose = {};
    await get("get_interest_year", User.token).then(res => {
      year = res.result;
      if (!year.includes(now)) {
        year.unshift({
          value: now
        });
      }
    });
    await get("get_count_close", User.token).then(res => {
      close = res.result;
    });
    await get("get_count_warm", User.token).then(res => {
      warm = res.result;
    });
    await get("get_count_cool", User.token).then(res => {
      cool = res.result;
    });
    await get("get_count_hot", User.token).then(res => {
      hot = res.result;
    });
    await get("get_close_in_month", User.token).then(res => {
      resClose = res.result;
      let dataClose = {};
      year.map((el, i) => {
        dataClose[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      });
      for (const key in dataClose) {
        if (dataClose.hasOwnProperty(key)) {
          resClose.map((el, i) => {
            if (resClose[i]) {
              if (resClose[i].year == key) {
                dataClose[key][resClose[i].month - 1] = resClose[i].close;
              }
            }
          });
        }
        dataInMonth[key] = { close: dataClose[key] };
      }
    });
    await get("get_unclose_in_month", User.token).then(res => {
      resUnclose = res.result;
      let dataUnclose = {};
      year.map((el, i) => {
        dataUnclose[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      });
      for (const key in dataUnclose) {
        if (dataUnclose.hasOwnProperty(key)) {
          resUnclose.map((el, i) => {
            if (resUnclose[i]) {
              if (resUnclose[i].year == key) {
                dataUnclose[key][resUnclose[i].month - 1] = resUnclose[i].unclose;
              }
            }
          });
        }
        dataInMonth[key] = { ...dataInMonth[key], unclose: dataUnclose[key] };
      }
    });
    await get("get_sale_interest", User.token).then(res => {
      sale = res.result;
      sale.unshift({
        sale_id: 0,
        value: "ทั้งหมด",
        year: year,
        close: resClose,
        unclose: resUnclose
      });
    });
    this.setState({ data: dataInMonth[now] });
  };

  _get_sale_data = async () => {
    await get("get_interest_year", User.token).then(res => {
      year = res.result;
      if (!year.includes(now)) {
        year.unshift({
          value: now
        });
      }
    });
    await get("get_count_close", User.token).then(res => {
      close = res.result;
    });
    await get("get_count_warm", User.token).then(res => {
      warm = res.result;
    });
    await get("get_count_cool", User.token).then(res => {
      cool = res.result;
    });
    await get("get_count_hot", User.token).then(res => {
      hot = res.result;
    });
    await get("get_close_in_month", User.token).then(res => {
      let data = {};
      year.map((el, i) => {
        data[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      });
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          month.map((el, i) => {
            if (res.result[i]) {
              if (res.result[i].year == key) {
                data[key][res.result[i].month - 1] = res.result[i].close;
              }
            }
          });
        }
        dataInMonth[key] = { close: data[key] };
      }
    });
    await get("get_unclose_in_month", User.token).then(res => {
      let data = {};
      year.map((el, i) => {
        data[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      });
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          month.map((el, i) => {
            if (res.result[i]) {
              if (res.result[i].year == key) {
                data[key][res.result[i].month - 1] = res.result[i].unclose;
              }
            }
          });
        }
        dataInMonth[key] = { ...dataInMonth[key], unclose: data[key] };
      }
    });
    this.setState({ data: dataInMonth[now] });
  };

  _changeData = async id => {
    sale.forEach((el, index) => {
      if (sale[index].sale_id === id) {
        let dataClose = {};
        let dataUnclose = {};
        year.map((el, i) => {
          dataClose[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
          dataUnclose[el.value] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        });
        for (const key in dataClose) {
          if (dataClose.hasOwnProperty(key)) {
            sale[index].close.map((el, i) => {
              if (sale[index].close[i]) {
                if (sale[index].close[i].year == key) {
                  dataClose[key][sale[index].close[i].month - 1] = sale[index].close[i].close;
                }
              }
            });
            sale[index].unclose.map((el, i) => {
              if (sale[index].unclose[i]) {
                if (sale[index].unclose[i].year == key) {
                  dataUnclose[key][sale[index].unclose[i].month - 1] = sale[index].unclose[i].unclose;
                }
              }
            });
          }
          dataInMonth[key] = {
            close: dataClose[key],
            unclose: dataUnclose[key]
          };
        }
      }
    });
    this.setState({ data: dataInMonth[now], selectYear: now });
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <View style={styles.flexWrap}>
          <View style={styles.box}>
            <View style={[styles.headView, { backgroundColor: color.Red }]}>
              <Image style={styles.icon} source={require("../assets/icon/hot_white.png")} />
              <Text style={styles.text}>เร่งปิดการขาย</Text>
            </View>
            <View style={[styles.headView, { justifyContent: "space-around" }]}>
              <Text style={[styles.text, { color: "black" }]}>{hot}</Text>
              <Text style={[styles.text, { color: "grey" }]}>คน</Text>
            </View>
          </View>

          <View style={styles.box}>
            <View style={[styles.headView, { backgroundColor: color.Orange }]}>
              <Image style={styles.icon} source={require("../assets/icon/warm_white.png")} />
              <Text style={styles.text}>การขายปานกลาง</Text>
            </View>
            <View style={[styles.headView, { justifyContent: "space-around" }]}>
              <Text style={[styles.text, { color: "black" }]}>{warm}</Text>
              <Text style={[styles.text, { color: "grey" }]}>คน</Text>
            </View>
          </View>

          <View style={styles.box}>
            <View style={[styles.headView, { backgroundColor: color.MinMain }]}>
              <Image style={styles.icon} source={require("../assets/icon/cold_white.png")} />
              <Text style={styles.text}>การขายปกติ</Text>
            </View>
            <View style={[styles.headView, { justifyContent: "space-around" }]}>
              <Text style={[styles.text, { color: "black" }]}>{cool}</Text>
              <Text style={[styles.text, { color: "grey" }]}>คน</Text>
            </View>
          </View>

          <View style={styles.box}>
            <View style={[styles.headView, { backgroundColor: color.SubGreen }]}>
              <Image style={styles.icon} source={require("../assets/icon/closing_white.png")} />
              <Text style={styles.text}>ปิดการขาย</Text>
            </View>
            <View style={[styles.headView, { justifyContent: "space-around" }]}>
              <Text style={[styles.text, { color: "black" }]}>{close}</Text>
              <Text style={[styles.text, { color: "grey" }]}>คน</Text>
            </View>
          </View>
        </View>
        <View style={styles.headContainer}>
          <Text style={styles.headText}>สรุปการปิดการขายและยอดของพนักงาน</Text>
        </View>
        <View
          style={[
            styles.headContainer,
            {
              height: null,
              justifyContent: "space-around",
              flexDirection: "row",
              paddingHorizontal: normalize(10),
              paddingBottom: normalize(10)
            }
          ]}
        >
          {User.role === "admin" ? (
            <Dropdown
              data={sale}
              label="เลือกพนักงาน"
              style={{ color: "white" }}
              baseColor="white"
              itemCount={5}
              fontFamily={font.medium}
              fontSize={normalize(14)}
              labelFontSize={normalize(12)}
              textAlign={"center"}
              selectedItemColor={color.Main}
              floatingLabelEnabled={true}
              containerStyle={{
                marginTop: normalize(-15),
                width: width * 0.6
              }}
              overlayStyle={{
                marginTop: aspectRatio < 1.6 ? normalize(60) : normalize(80)
              }}
              onChangeText={(value, index, data) => {
                this._changeData(data[index].sale_id);
              }}
            />
          ) : null}
          <Dropdown
            data={year}
            value={this.state.selectYear}
            label="เลือก พ.ศ."
            style={{ color: "white" }}
            baseColor="white"
            itemCount={3}
            fontFamily={font.medium}
            fontSize={normalize(14)}
            labelFontSize={normalize(12)}
            textAlign={"center"}
            selectedItemColor={color.Main}
            floatingLabelEnabled={true}
            containerStyle={{
              marginTop: normalize(-15),
              width: User.role === "admin" ? width * 0.3 : width * 0.9
            }}
            overlayStyle={{
              marginTop: aspectRatio < 1.6 ? normalize(60) : normalize(80)
            }}
            onChangeText={(value, index, data) => {
              this.setState({ data: dataInMonth[value], selectYear: value });
            }}
          />
        </View>

        <View style={{ flex: 1 }}>
          {this.state.data ? (
            <ChartView
              style={{
                flex: 1,
                zIndex: 1
              }}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              config={conf(this.state.data)}
              originWhitelist={[""]}
            />
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={[
                  styles.text,
                  {
                    color: color.SubMain,
                    zIndex: 1
                  }
                ]}
              >
                ไม่มีข้อมูล
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  flexWrap: {
    justifyContent: "center",
    marginVertical: normalize(10),
    flexDirection: "row",
    flexWrap: "wrap"
  },
  text: {
    color: "white",
    fontSize: normalize(14)
  },
  icon: {
    marginRight: normalize(5),
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  box: {
    borderWidth: normalize(0.5),
    borderColor: "#eee",
    flexDirection: "column",
    margin: normalize(2),
    backgroundColor: "white",
    shadowOpacity: 0.5,
    shadowColor: "lightgrey",
    shadowOffset: { height: 5, width: 5 },
    elevation: 5
  },
  headView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: aspectRatio ? height * 0.06 : height * 0.07,
    width: width / 2 - normalize(10),
    backgroundColor: "white"
  },
  headContainer: {
    backgroundColor: color.Main,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: normalize(40),
    width: width,
    marginBottom: normalize(-0.5)
  },
  headText: {
    fontSize: normalize(14),
    fontFamily: font.bold,
    color: "white"
  },
  label: {
    fontWeight: null,
    fontSize: normalize(11),
    color: "white"
  },
  title: {
    top: normalize(10),
    left: 0,
    right: 0,
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  titleText: {
    color: "grey",
    fontSize: normalize(10)
  },
  circle: {
    marginHorizontal: normalize(6),
    width: normalize(8),
    height: normalize(8),
    borderRadius: normalize(4)
  }
  // triangleDown: {
  //   width: 0,
  //   height: 0,
  //   zIndex: 99,
  //   alignSelf: "center",
  //   position: "absolute",
  //   borderLeftWidth: normalize(10),
  //   borderRightWidth: normalize(10),
  //   borderTopWidth: normalize(10),
  //   borderLeftColor: "transparent",
  //   borderRightColor: "transparent",
  //   borderTopColor: color.Main
  // }
});
