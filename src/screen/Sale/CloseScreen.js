import React from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  StyleSheet,
  Dimensions,
  FlatList,
  View,
  Image,
  Text
} from "react-native";
import call from "react-native-phone-call";
import Swipeable from "react-native-swipeable";

import sale from "../../statics/sale";
import color from "../../statics/color";
import normalize from "../../assets/normalize";
import { SearchBar } from "react-native-elements";
import font from "../../assets/font";

import { get, post } from "../../services/index";
import User from "../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class CloseScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      data: sale,
      loading: true
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_close_job", User.token).then(res => {
      this.setState({
        data: res.result
      });
    });
  };

  _recenter(index) {
    if (this[index.toString()]) {
      this[index.toString()].recenter();
    }
  }

  _setSearchText(event) {
    let data = sale;
    let searchText = event.trim().toLowerCase();
    let res = data.filter(l => {
      return (l.c_name.toLowerCase() + " " + l.c_lastname.toLowerCase()).match(searchText);
    });
    this.setState({ data: res });
  }

  _logCall = async (interest_id, phoneNumber) => {
    object = {
      interest_id: interest_id,
      phoneNumber: phoneNumber,
      callType: "OUTGOING",
      callDuration: 0
    };
    await post("create_call", object, User.token).then(res => {
      let data = res.result;
    });
  };

  _goContact(id, name, lastname) {
    this.props.navigation.navigate("CareContact", {
      id: id,
      name: name,
      lastname: lastname
    });
  }

  _renderItem = (item, index) => {
    let args = {
      number: item.c_phone,
      prompt: false
    };

    const rightButtons = [
      <TouchableOpacity
        style={[styles.rightSwipeItem, { backgroundColor: color.SubMain }]}
        onPress={() => {
          this._recenter(index);
          this._goContact(item.interest_id, item.c_name, item.c_lastname);
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../assets/icon_mobile/calling.png")} />
          <Text style={styles.label}>การติดต่อ</Text>
        </View>
      </TouchableOpacity>,
      <TouchableOpacity
        style={styles.rightSwipeItem}
        onPress={() => {
          call(args).catch(console.error);
          User.role === "sale" ? this._logCall(item.interest_id, item.phone) : null;
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../assets/icon_mobile/phone.png")} />
          <Text style={styles.label}>โทรออก</Text>
        </View>
      </TouchableOpacity>
    ];

    return (
      <Swipeable
        key={index}
        onRef={ref => (this[index.toString()] = ref)}
        rightButtonWidth={aspectRatio < 1.6 ? width * 0.15 : width * 0.2}
        rightButtons={rightButtons}
      >
        <View style={styles.listContainer}>
          <Text style={styles.name}>
            {item.c_name} {item.c_lastname}
          </Text>
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                fontSize: normalize(12),
                marginTop: aspectRatio < 1.6 ? normalize(-5) : normalize(-2),
                marginLeft: aspectRatio < 1.6 ? normalize(30) : normalize(10),
                color: color.SubMain
              }}
            >
              {item.product_name}
              <Image style={styles.icon} source={require("../../assets/icon_mobile/line.png")} />
              {item.c_phone}
            </Text>
          </View>
        </View>
      </Swipeable>
    );
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={styles.container}>
        <SearchBar
          round
          placeholder="ค้นหา"
          onChangeText={text => this._setSearchText(text)}
          onClearText={null}
          inputStyle={{
            backgroundColor: "#eee",
            fontFamily: font.medium,
            fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(14)
          }}
          containerStyle={{
            backgroundColor: "white",
            borderTopColor: "lightgrey",
            borderBottomColor: "white"
          }}
        />
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this._renderItem(item, index)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listContainer: {
    flexDirection: "column",
    justifyContent: "center",
    height: aspectRatio < 1.6 ? normalize(50) : normalize(60),
    borderBottomWidth: 0.5,
    backgroundColor: "white",
    borderColor: "lightgrey"
  },
  swipeable: {
    width: aspectRatio < 1.6 ? width * 0.15 : width * 0.2,
    justifyContent: "center",
    alignItems: "center"
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: "center",
    borderColor: "lightgrey",
    borderBottomWidth: 0.5,
    backgroundColor: color.Main
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconPhone: {
    height: aspectRatio < 1.6 ? normalize(18) : normalize(22),
    width: aspectRatio < 1.6 ? normalize(18) : normalize(22),
    resizeMode: "contain"
  },
  name: {
    color: color.Main,
    marginVertical: normalize(-1),
    marginLeft: aspectRatio < 1.6 ? normalize(30) : normalize(10),
    fontSize: normalize(14)
  },
  label: {
    fontSize: aspectRatio < 1.6 ? normalize(12) : normalize(10),
    marginTop: normalize(5),
    color: "white",
    alignItems: "center"
  }
});

AppRegistry.registerComponent("CloseScreen", () => CloseScreen);
