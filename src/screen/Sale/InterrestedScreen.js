import React from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  RefreshControl,
  AppRegistry,
  StyleSheet,
  Dimensions,
  FlatList,
  Alert,
  Image,
  View,
  Text
} from "react-native";
import Swipeable from "react-native-swipeable";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";

import User from "../../mobx/user";
import font from "../../assets/font";
import color from "../../statics/color";
import normalize from "../../assets/normalize";
import { get, post } from "../../services/index";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
var data = [];
let category = [];

export default class InterrestedScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      name: "",
      category: "",
      refreshing: false,
      customer: []
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_wait_interest", User.token).then(res => {
      data = res.result;
      for (let index = 0; index < data.length; index++) {
        if (data[index].c_organization === null) {
          data[index].c_organization = "ไม่ระบุ";
        }
      }
      this.setState({
        customer: res.result
      });
    });

    await get("get_customer_organization", User.token).then(res => {
      category = res.result;
      for (let index = 0; index < category.length; index++) {
        if (category[index].value === null) {
          category[index].value = "ไม่ระบุ";
        }
      }
    });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (nextProps.data !== this.props.data) {
      this.onRefresh(nextProps.data);
      return true;
    }
    return true;
  };

  onRefresh(data) {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false, data });
    }, 1000);
  }

  _recenter(index) {
    if (this[index.toString()]) {
      this[index.toString()].recenter();
    }
  }

  _setSearch(name, category) {
    let searchName = name.trim().toLowerCase();
    let searchCategory = category.trim().toLowerCase();

    let resName = data.filter(l => {
      return (l.c_name.toLowerCase() + " " + l.c_lastname.toLowerCase()).match(searchName);
    });

    let resCategory = resName.filter(l => {
      return l.c_organization.toLowerCase().match(searchCategory);
    });

    this.setState({ customer: resCategory });
  }

  _addToSale = async interest_id => {
    let obj = { interest_id: interest_id };
    await post("add_wait_interest", obj, User.token).then(res => {
      setTimeout(() => {
        this._fetch();
        Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
      }, 200);
    });
  };

  _renderItem = (item, index) => {
    const rightButtons = [
      <TouchableOpacity
        style={styles.rightSwipeItem}
        onPress={() => {
          this._recenter(index);
          this._addToSale(item.interest_id);
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../assets/icon_mobile/add.png")} />
          <Text style={styles.label}>เพิ่มลูกค้า</Text>
        </View>
      </TouchableOpacity>
    ];

    return (
      <Swipeable
        key={index}
        onRef={ref => (this[index.toString()] = ref)}
        rightButtonWidth={aspectRatio < 1.6 ? width * 0.15 : width * 0.2}
        rightButtons={rightButtons}
      >
        <View style={styles.listContainer}>
          <Text style={styles.name}>
            {item.c_name} {item.c_lastname}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                fontSize: normalize(12),
                marginTop: aspectRatio < 1.6 ? normalize(-5) : normalize(-2),
                marginLeft: aspectRatio < 1.6 ? normalize(30) : normalize(10),
                color: color.SubMain
              }}
            >
              {item.product_name ? item.product_name : "ไม่ระบุ"}
              <Image style={styles.icon} source={require("../../assets/icon_mobile/line.png")} />
              {item.c_phone ? item.c_phone : "ไม่ระบุ"}
            </Text>
          </View>
        </View>
      </Swipeable>
    );
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={styles.container}>
        <SearchBar
          round
          placeholder="ค้นหา"
          onChangeText={text => {
            this.setState({
              name: text
            });
            this._setSearch(text, this.state.category);
          }}
          onClearText={null}
          inputStyle={{
            backgroundColor: "#eee",
            fontFamily: font.medium,
            fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(14)
          }}
          containerStyle={{
            backgroundColor: "white",
            borderTopColor: "lightgrey",
            borderBottomColor: "white"
          }}
        />
        <View
          style={{
            paddingBottom: normalize(10),
            borderBottomColor: "lightgrey",
            borderBottomWidth: normalize(1)
          }}
        >
          <Dropdown
            label="เลือกประเภท"
            itemCount={5}
            data={category}
            fontSize={normalize(14)}
            baseColor={color.SubMain}
            floatingLabelEnabled={true}
            labelFontSize={normalize(14)}
            selectedItemColor={color.Main}
            style={{ color: color.SubMain }}
            inputContainerStyle={{
              borderBottomColor: color.SubMain,
              borderBottomWidth: 0.9
            }}
            containerStyle={{
              width: width,
              ...(aspectRatio < 1.6
                ? {
                    marginBottom: null,
                    marginTop: null,
                    paddingHorizontal: normalize(10)
                  }
                : {
                    marginBottom: normalize(-10),
                    marginTop: normalize(-15),
                    paddingHorizontal: normalize(19)
                  })
            }}
            overlayStyle={{
              marginTop: normalize(55)
            }}
            onChangeText={res => {
              this.setState({
                category: res
              });
              this._setSearch(this.state.name, res);
            }}
          />
        </View>
        <FlatList
          data={this.state.customer}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          refreshControl={
            <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.onRefresh(this.state.data)} />
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listContainer: {
    flexDirection: "column",
    justifyContent: "center",
    height: aspectRatio < 1.6 ? normalize(50) : normalize(60),
    borderBottomWidth: 0.5,
    backgroundColor: "white",
    borderColor: "lightgrey"
  },
  swipeable: {
    width: aspectRatio < 1.6 ? width * 0.15 : width * 0.2,
    justifyContent: "center",
    alignItems: "center"
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: "center",
    borderColor: "lightgrey",
    borderBottomWidth: 0.5,
    backgroundColor: color.Main
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconPhone: {
    height: aspectRatio < 1.6 ? normalize(18) : normalize(22),
    width: aspectRatio < 1.6 ? normalize(18) : normalize(22),
    resizeMode: "contain"
  },
  name: {
    color: color.Main,
    marginVertical: normalize(-1),
    marginLeft: aspectRatio < 1.6 ? normalize(30) : normalize(10),
    fontSize: normalize(14)
  },
  label: {
    fontSize: aspectRatio < 1.6 ? normalize(12) : normalize(10),
    marginTop: normalize(5),
    color: "white",
    alignItems: "center"
  }
});

AppRegistry.registerComponent("InterrestedScreen", () => InterrestedScreen);
