import React from "react";
import { TouchableOpacity, RefreshControl, StyleSheet, Dimensions, FlatList, View, Image, Text } from "react-native";
import call from "react-native-phone-call";
import Swipeable from "react-native-swipeable";
import { SearchBar } from "react-native-elements";
import { withNavigation } from "react-navigation";

import ModalAddProduct from "../../../components/ModalAddProduct";
import ModalEditStatus from "../../../components/ModalEditStatus";

import { normalize } from "react-native-elements";
import { post } from "../../../services/index";
import color from "../../../statics/color";
import font from "../../../assets/font";
import User from "../../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
let data = [];

class Warm extends React.Component {
  constructor() {
    super();
    this.state = {
      customer_id: null,
      interest_id: null,
      status: null,
      showModalAdd: false,
      showModalEdit: false,
      refreshing: false,
      data: []
    };
  }

  componentDidMount = () => {
    data = this.props.data;
    this.setState({ data: this.props.data });
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (nextProps.data !== this.props.data) {
      this.onRefresh(nextProps.data);
      return true;
    }
    return true;
  };

  onRefresh(data) {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false, data });
    }, 1000);
  }

  _showModalAdd() {
    this.setState({
      showModalAdd: true
    });
  }

  _dismissModalAdd() {
    this.props._fetch();
    this.setState({
      showModalAdd: false
    });
  }

  _showModalEdit() {
    this.setState({
      showModalEdit: true
    });
  }

  _dismissModalEdit() {
    this.setState({
      showModalEdit: false
    });
  }

  _setSearch(event) {
    let searchText = event.trim().toLowerCase();
    let res = data.filter(l => {
      return (l.name.toLowerCase() + " " + l.lastname.toLowerCase()).match(searchText);
    });
    this.setState({ data: res });
  }

  _logCall = async (interest_id, phoneNumber) => {
    object = {
      interest_id: interest_id,
      phoneNumber: phoneNumber,
      callType: "OUTGOING",
      callDuration: 0
    };
    await post("create_call", object, User.token).then(res => {
      let data = res.result;
    });
  };

  _goContact(id, name, lastname) {
    this.props.navigation.navigate("CareContact", {
      id: id,
      name: name,
      lastname: lastname
    });
  }

  _recenter(index) {
    if (this[index.toString()]) {
      this[index.toString()].recenter();
    }
  }

  _renderItem = (item, index) => {
    let args = {
      number: item.phone,
      prompt: false
    };

    const rightButtons = [
      <TouchableOpacity
        style={[styles.rightSwipeItem, { backgroundColor: "#73abe4" }]}
        onPress={() => {
          this._recenter(index);
          this._showModalEdit();
          this.setState({ interest_id: item.interest_id, status: item.status });
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../../assets/icon_mobile/edit.png")} />
          <Text style={styles.label}>แก้ไขสถานะ</Text>
        </View>
      </TouchableOpacity>,
      <TouchableOpacity
        style={[styles.rightSwipeItem, { backgroundColor: "#578ed0" }]}
        onPress={() => {
          this._recenter(index);
          this._showModalAdd();
          this.setState({ customer_id: item.customer_id });
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../../assets/icon_mobile/add_product_customer.png")} />
          <Text style={styles.label}>เพิ่มสินค้า</Text>
        </View>
      </TouchableOpacity>,
      <TouchableOpacity
        style={[styles.rightSwipeItem, { backgroundColor: "#2a79b9" }]}
        onPress={() => {
          this._recenter(index);
          this._goContact(item.interest_id, item.name, item.lastname);
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../../assets/icon_mobile/calling.png")} />
          <Text style={styles.label}>การติดต่อ</Text>
        </View>
      </TouchableOpacity>,
      <TouchableOpacity
        style={styles.rightSwipeItem}
        onPress={() => {
          call(args).catch(console.error);
          User.role === "sale" ? this._logCall(item.interest_id, item.phone) : null;
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../../assets/icon_mobile/phone.png")} />
          <Text style={styles.label}>โทรออก</Text>
        </View>
      </TouchableOpacity>
    ];

    return (
      <Swipeable
        key={index}
        onRef={ref => (this[index.toString()] = ref)}
        rightButtonWidth={aspectRatio < 1.6 ? width * 0.15 : width * 0.2}
        rightButtons={rightButtons}
      >
        <View style={styles.listContainer}>
          <Text
            style={[
              styles.name,
              {
                marginLeft: aspectRatio < 1.6 ? normalize(50) : normalize(10),
                marginVertical: normalize(-1)
              }
            ]}
          >
            {item.name} {item.lastname}
          </Text>
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                fontSize: aspectRatio < 1.6 ? normalize(16) : normalize(12),
                marginTop: aspectRatio < 1.6 ? normalize(-5) : normalize(-2),
                marginLeft: aspectRatio < 1.6 ? normalize(50) : normalize(10),
                color: color.SubMain
              }}
            >
              {item.product_name}
              <Image style={styles.icon} source={require("../../../assets/icon_mobile/line.png")} />
              {item.phone}
            </Text>
          </View>
        </View>
      </Swipeable>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <SearchBar
          round
          placeholder="ค้นหา"
          onChangeText={text => this._setSearch(text)}
          onClearText={null}
          inputStyle={{
            backgroundColor: "#eee",
            fontFamily: font.medium,
            fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(14)
          }}
          containerStyle={{
            backgroundColor: "white",
            borderTopColor: "lightgrey",
            borderBottomColor: "white"
          }}
        />
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this._renderItem(item, index)}
          refreshControl={
            <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.onRefresh(this.state.data)} />
          }
        />

        {this.state.showModalAdd ? (
          <ModalAddProduct
            visible={this.state.showModalAdd}
            dismissModal={this._dismissModalAdd.bind(this)}
            customer_id={this.state.customer_id}
          />
        ) : null}

        {this.state.showModalEdit ? (
          <ModalEditStatus
            visible={this.state.showModalEdit}
            _fetch={this.props._fetch.bind(this)}
            dismissModal={this._dismissModalEdit.bind(this)}
            interest_id={this.state.interest_id}
            status={this.state.status}
          />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listContainer: {
    flexDirection: "column",
    justifyContent: "center",
    height: aspectRatio < 1.6 ? normalize(60) : normalize(50),
    borderBottomWidth: 0.5,
    backgroundColor: "white",
    borderColor: "lightgrey"
  },
  swipeable: {
    width: aspectRatio < 1.6 ? width * 0.15 : width * 0.2,
    justifyContent: "center",
    alignItems: "center"
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: "center",
    borderColor: "lightgrey",
    borderBottomWidth: 0.5,
    backgroundColor: color.Main
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconPhone: {
    height: normalize(22),
    width: normalize(22),
    resizeMode: "contain"
  },
  name: {
    color: color.Main,
    fontSize: aspectRatio < 1.6 ? normalize(16) : normalize(14)
  },
  label: {
    fontSize: aspectRatio < 1.6 ? normalize(12) : normalize(10),
    marginTop: normalize(5),
    color: "white",
    alignItems: "center"
  }
});

export default withNavigation(Warm);
