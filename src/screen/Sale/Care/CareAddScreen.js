import React from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  Dimensions,
  StyleSheet,
  ScrollView,
  FlatList,
  Alert,
  Image,
  View,
  Text
} from "react-native";
import { Dropdown } from "react-native-material-dropdown";
import { FormLabel, FormInput } from "react-native-elements";

import HeadBar from "../../../components/HeadBar";

import { get, post } from "../../../services/index";
import normalize from "../../../assets/normalize";
import color from "../../../statics/color";
import font from "../../../assets/font";
import User from "../../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class CareAddScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      status: [],
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      lineId: "",
      facebook: "",
      address: "",
      organization: "",
      item_product_id: null,
      item_product_name: "",
      item_product_detail: "",
      item_status: "",
      addProduct: [],
      loading: true
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_product", User.token).then(res => {
      this.setState({ product: res.result });
    });
    await get("get_all_status", User.token).then(res => {
      res.result.forEach(el => {
        if (el.status === "hot") {
          el.value = "เร่งปิดการขาย";
        } else if (el.status === "warm") {
          el.value = "การขายปานกลาง";
        } else if (el.status === "cool") {
          el.value = "การขายปกติ";
        } else if (el.status === "wait") {
          el.value = "รอดำเนินการ";
        }
      });
      this.setState({ status: res.result });
    });
  };

  _addProduct = () => {
    let { addProduct, item_product_id, item_product_name, item_product_detail, item_status } = this.state;

    function notEqual(value, index, array) {
      return value.product_id !== item_product_id;
    }

    if (addProduct.every(notEqual) && item_product_id !== null && item_status !== "") {
      this.setState({
        addProduct: [
          ...addProduct,
          {
            product_id: item_product_id,
            product_name: item_product_name,
            product_detail: item_product_detail,
            status_name: item_status,
            status:
              item_status === "hot"
                ? 1
                : item_status === "warm"
                ? 2
                : item_status === "cool"
                ? 3
                : item_status === "wait"
                ? 0
                : null
          }
        ]
      });
    }
  };

  _addCustomer = () => {
    let { addProduct, firstname, lastname, email, phone, lineId, facebook, address, organization } = this.state;
    let obj = {
      contact: {
        name: firstname,
        lastname: lastname,
        email: email,
        line: lineId,
        phone: phone,
        facebook: facebook,
        address: address,
        organization: organization
      },
      product: addProduct
    };
    if (
      firstname !== "" &&
      lastname !== "" &&
      email !== "" &&
      phone !== "" &&
      lineId !== "" &&
      facebook !== "" &&
      address !== "" &&
      organization !== ""
    ) {
      post("create_customer_from_sale", obj, User.token).then(res => {
        if (res.success) {
          this.props.navigation.goBack();
        }
        setTimeout(() => {
          Alert.alert(res.success === true ? "สำเร็จ" : "ผิดพลาด", res.message);
        }, 200);
      });
    } else {
      Alert.alert("ผิดพลาด", "กรุณากรอกข้อมูลให้ครบถ้วน");
    }
  };

  _renderItem = item => {
    return item.status !== "close" ? (
      <View style={styles.listContainer}>
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              height: normalize(40),
              width: normalize(40)
            }}
          >
            <Image
              style={{
                height: normalize(25),
                width: normalize(25),
                resizeMode: "contain"
              }}
              source={require("../../../assets/icon_mobile/select.png")}
            />
          </View>
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <Text style={styles.name}>{item.product_name}</Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Text numberOfLines={1} style={styles.description}>
                {item.product_detail}
              </Text>
              {item.status_name === "hot" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image style={styles.iconStatus} source={require("../../../assets/icon/hot_grey.png")} />
                  <Text style={styles.status}>เร่งปิดการขาย</Text>
                </View>
              ) : null}
              {item.status_name === "warm" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image style={styles.iconStatus} source={require("../../../assets/icon/warm_grey.png")} />
                  <Text style={styles.status}>การขายปานกลาง</Text>
                </View>
              ) : null}
              {item.status_name === "cool" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image style={styles.iconStatus} source={require("../../../assets/icon/cold_grey.png")} />
                  <Text style={styles.status}>การขายปกติ</Text>
                </View>
              ) : null}
              {item.status_name === "wait" ? (
                <View
                  style={{
                    flexDirection: "row",
                    left: normalize(5)
                  }}
                >
                  <Image style={styles.iconStatus} source={require("../../../assets/icon/wait_grey.png")} />
                  <Text style={styles.status}>รอดำเนินการ</Text>
                </View>
              ) : null}
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: normalize(40),
            width: normalize(40)
          }}
        >
          <Image
            style={{
              height: normalize(20),
              width: normalize(20),
              resizeMode: "contain"
            }}
            source={require("../../../assets/icon_mobile/icon_delete.png")}
          />
        </TouchableOpacity>
      </View>
    ) : null;
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <ScrollView style={{ backgroundColor: "white" }}>
        <HeadBar eng={"ข้อมูลลูกค้า"} visible={false} />
        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>ชื่อจริง</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกชื่อจริง"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                firstname: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>นามสกุล</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกนามสกุล"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                lastname: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>E-mail</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอก E-mail"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                email: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>หมายเลขโทรศัพท์</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกหมายเลขโทรศัพท์"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                phone: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>LINE ID</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอก LINE ID"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                lineId: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>Facebook</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอก Facebook"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                facebook: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>ที่อยู่</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกที่อยู่"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                address: res
              });
            }}
          />
          <FormLabel labelStyle={styles.label}>ชื่อบริษัท</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกชื่อบริษัท"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                organization: res
              });
            }}
          />
        </View>
        <HeadBar eng={"สินค้าที่สนใจ"} visible={false} />
        <View style={styles.container}>
          <Dropdown
            label="สินค้า"
            itemCount={5}
            data={this.state.product}
            baseColor={color.SubMain}
            itemColor={"lightgrey"}
            fontSize={normalize(14)}
            style={{ color: color.SubMain }}
            selectedItemColor={color.SubMain}
            floatingLabelEnabled={true}
            labelFontSize={normalize(14)}
            inputContainerStyle={{
              borderBottomColor: color.SubMain,
              borderBottomWidth: 1
            }}
            containerStyle={{
              marginTop: aspectRatio < 1.6 ? normalize(10) : normalize(0)
            }}
            overlayStyle={{
              marginTop: normalize(50)
            }}
            onChangeText={(value, index, data) => {
              this.setState({
                item_product_id: data[index].id_product,
                item_product_name: data[index].product_name,
                item_product_detail: data[index].product_detail
              });
            }}
          />
          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <Dropdown
              label="สถานะการขาย"
              itemCount={5}
              data={this.state.status}
              baseColor={color.SubMain}
              itemColor={"lightgrey"}
              fontSize={normalize(14)}
              style={{ color: color.SubMain }}
              selectedItemColor={color.SubMain}
              floatingLabelEnabled={true}
              labelFontSize={normalize(14)}
              inputContainerStyle={{
                borderBottomColor: color.SubMain,
                borderBottomWidth: 1
              }}
              containerStyle={{
                width: width * 0.55,
                marginTop: aspectRatio < 1.6 ? normalize(10) : normalize(0)
              }}
              overlayStyle={{
                marginTop: normalize(50)
              }}
              onChangeText={(value, index, data) => {
                this.setState({ item_status: data[index].status });
              }}
            />

            <TouchableOpacity style={styles.button} onPress={() => this._addProduct()}>
              <Text style={styles.buttonText}>เพิ่มสินค้า</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: normalize(15),
              marginBottom: normalize(5),
              borderColor: "lightgrey",
              borderBottomWidth: normalize(0.5)
            }}
          >
            <Text
              style={{
                borderBottomColor: "lightgrey",
                borderBottomWidth: normalize(0.5),
                fontSize: normalize(14),
                color: color.SubMain
              }}
            >
              รายการสินค้าที่สนใจ
            </Text>
          </View>
          <FlatList
            style={{ minHeight: normalize(50) }}
            data={this.state.addProduct}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => this._renderItem(item)}
          />
          <TouchableOpacity style={styles.buttonBlue} onPress={() => this._addCustomer()}>
            <Text style={styles.buttonText}>+ เพิ่มลูกค้า</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20)
  },
  containerInput: {
    
    marginVertical: aspectRatio < 1.6 ? normalize(5) : normalize(-10),
    borderBottomColor: color.SubMain
  },
  inputView: {
    // display: "flex",
    // alignSelf: "center",
    ...(aspectRatio < 1.6
      ? {
          paddingTop: normalize(10),
          paddingBottom: normalize(20)
        }
      : {
          paddingTop: normalize(5),
          paddingBottom: normalize(20)
        })
  },
  listContainer: {
    height: normalize(40),
    margin: normalize(2),
    flexDirection: "row",
    borderWidth: 0.5,
    borderRadius: normalize(2),
    borderColor: "lightgrey",
    justifyContent: "space-between"
  },
  label: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  },
  name: {
    marginVertical: normalize(-2),
    fontSize: normalize(12),
    fontFamily: font.bold,
    color: color.Main
  },
  description: {
    width: width * 0.4,
    marginVertical: normalize(-2),
    fontSize: normalize(10),
    color: color.SubMain
  },
  iconStatus: {
    marginBottom: normalize(-5),
    marginRight: normalize(5),
    height: normalize(10),
    width: normalize(10),
    resizeMode: "contain"
  },
  status: {
    marginVertical: normalize(-2),
    fontSize: normalize(10),
    color: "lightgrey"
  },
  button: {
    width: width / 3.5,
    marginTop: normalize(10),
    backgroundColor: color.SubGreen,
    borderRadius: normalize(40),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    padding: normalize(10)
  },
  buttonBlue: {
    alignSelf: "center",
    alignItems: "center",
    backgroundColor: color.Main,
    padding: normalize(10),
    margin: normalize(10),
    width: width * 0.9,
    borderRadius: normalize(50)
  },
  buttonText: {
    fontSize: normalize(14),
    color: "white"
  }
});

AppRegistry.registerComponent("CareAddScreen", () => CareAddScreen);
