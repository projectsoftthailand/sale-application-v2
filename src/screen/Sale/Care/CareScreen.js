import React, { Component } from "react";
import { ActivityIndicator, AppRegistry, StyleSheet, Dimensions, Image, View, Text } from "react-native";
import { TabView, TabBar } from "react-native-tab-view";

import normalize from "../../../assets/normalize";
import { get, post } from "../../../services/index";
import color from "../../../statics/color";
import User from "../../../mobx/user";

import StatusScreen from "./StatusScreen";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
var dataWarm = [];
var dataCool = [];
var dataHot = [];

export default class CareScreen extends Component {
  constructor() {
    super();
    this.state = {
      index: 0,
      totalClose: 0,
      totalCallOut: 0,
      totalCallIn: 0,
      dataWarm: [],
      dataCool: [],
      dataHot: [],
      routes: [
        {
          key: "0",
          title: "เร่งปิดการขาย",
          icon: require("../../../assets/icon/hot_active.png")
        },
        {
          key: "1",
          title: "การขายปานกลาง",
          icon: require("../../../assets/icon/warm_active.png")
        },
        {
          key: "2",
          title: "การขายปกติ",
          icon: require("../../../assets/icon/cold_active.png")
        }
      ],
      loading: true
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    dataWarm = [];
    dataCool = [];
    dataHot = [];
    object = { sale_id: User.user_id };
    await post("get_status_by_sale", object, User.token).then(res => {
      let data = res.result;
      data.forEach(el => {
        if (el.status_name === "hot") {
          dataHot.unshift(el);
        } else if (el.status_name === "warm") {
          dataWarm.unshift(el);
        } else if (el.status_name === "cool") {
          dataCool.unshift(el);
        }
      });
      this.setState({ dataHot, dataCool, dataWarm });
    });
    await get("get_count_close", User.token).then(res => {
      this.setState({ totalClose: res.result });
    });
    await get("get_count_call", User.token).then(res => {
      this.setState({ totalCallIn: res.countIn, totalCallOut: res.countOut });
    });
  };

  _renderScene = ({ route }) => {
    let { dataHot, dataCool, dataWarm } = this.state;
    switch (route.key) {
      case "0":
        return <StatusScreen data={dataHot} _fetch={this._fetch.bind(this)} />;
      case "1":
        return <StatusScreen data={dataWarm} _fetch={this._fetch.bind(this)} />;
      case "2":
        return <StatusScreen data={dataCool} _fetch={this._fetch.bind(this)} />;
      default:
        return null;
    }
  };

  _renderIcon = ({ route }) => {
    return <Image style={styles.iconStatus} source={route.icon} />;
  };

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        renderIcon={this._renderIcon}
        labelStyle={{
          marginHorizontal: normalize(0),
          fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(9),
          color: color.Main
        }}
        indicatorStyle={{
          backgroundColor: color.Main,
          height: normalize(3)
        }}
        tabStyle={{
          marginBottom: aspectRatio < 1.6 ? normalize(-5) : normalize(-10)
        }}
        style={{
          borderBottomWidth: normalize(1),
          borderTopWidth: normalize(1),
          backgroundColor: "white",
          borderColor: "#eee",
          elevation: 1
        }}
      />
    );
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this._renderScene}
          onIndexChange={index => this.setState({ index })}
          swipeEnabled={false}
        />
        <View style={styles.bottomView}>
          {/* <View style={styles.contentBottomView}>
            <Text style={[styles.textBottomView, { fontSize: normalize(16) }]}>
              {this.state.totalCallIn}
            </Text>
            <Text style={styles.textBottomView}>จำนวนการรับ</Text>
          </View> */}
          <View style={styles.contentBottomView}>
            <Text style={[styles.textBottomView, { fontSize: normalize(16) }]}>{this.state.totalCallOut}</Text>
            <Text style={styles.textBottomView}>จำนวนการโทร</Text>
          </View>
          <View
            style={[
              styles.contentBottomView,
              {
                borderColor: null,
                borderRightWidth: null
              }
            ]}
          >
            <Text style={[styles.textBottomView, { fontSize: normalize(16) }]}>{this.state.totalClose}</Text>
            <Text style={styles.textBottomView}>จำนวนปิดการขาย</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    margin: normalize(15),
    width: normalize(90),
    height: normalize(90),
    borderWidth: normalize(0.5),
    borderRadius: normalize(45),
    borderColor: "lightgrey"
  },
  iconView: {
    justifyContent: "center",
    backgroundColor: color.Main,
    height: normalize(50),
    width: normalize(50),
    marginTop: normalize(35),
    marginRight: normalize(10),
    borderRadius: normalize(25)
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconPhone: {
    height: normalize(20),
    width: normalize(20),
    resizeMode: "contain",
    alignSelf: "center"
  },
  iconStatus: {
    marginBottom: normalize(-4),
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  bottomView: {
    position: "relative",
    bottom: 0,
    flexDirection: "row",
    height: aspectRatio < 1.6 ? normalize(55) : normalize(45),
    width: width
  },
  contentBottomView: {
    flexDirection: "column",
    borderColor: "lightgrey",
    borderRightWidth: normalize(0.5),
    backgroundColor: color.Main,
    alignItems: "center",
    justifyContent: "center",
    width: width / 2
  },
  textBottomView: {
    margin: normalize(-1),
    color: "white",
    fontSize: aspectRatio < 1.6 ? normalize(14) : normalize(12)
  }
});

AppRegistry.registerComponent("CareScreen", () => CareScreen);
