import React from "react";
import { ActivityIndicator, AppRegistry, StyleSheet, Dimensions, FlatList, View, Text } from "react-native";

import User from "../../../mobx/user";
import color from "../../../statics/color";
import { post } from "../../../services/index";
import normalize from "../../../assets/normalize";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class CareContactScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: []
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    let object = { interest_id: this.props.navigation.getParam("id") };
    await post("get_call", object, User.token).then(res => {
      this.setState({
        data: JSON.parse(res.result[0].time_call)
      });
    });
  };

  convertDate(inputFormat) {
    function pad(s) {
      return s < 10 ? "0" + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/");
  }

  convertTime(inputFormat) {
    function pad(s) {
      return s < 10 ? "0" + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getHours()), pad(d.getMinutes())].join(":");
  }

  _renderItem = item => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.column}>
          <Text style={styles.name}>
            {this.props.navigation.getParam("name")} {this.props.navigation.getParam("lastname")}
          </Text>
          <Text
            style={{
              fontSize: normalize(12),
              color: color.SubMain
            }}
          >
            {item.phoneNumber}
          </Text>
        </View>

        <View style={styles.column}>
          <Text
            style={{
              textAlign: "right",
              fontSize: normalize(12),
              color: color.SubMain
            }}
          >
            {this.convertDate(item.callDayTime)}
          </Text>
          <Text
            style={{
              textAlign: "right",
              fontSize: normalize(12),
              color: "lightgrey"
            }}
          >
            {this.convertTime(item.callDayTime)} น.
            {/* {Number(item.callDuration) + 1} นาที */}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={styles.container}>
        {typeof this.state.data[0] !== "undefined" ? (
          <FlatList
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => this._renderItem(item)}
          />
        ) : (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text style={styles.detail}>ไม่มีข้อมูล</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: aspectRatio < 1.6 ? normalize(50) : normalize(60),
    borderBottomWidth: 0.5,
    backgroundColor: "white",
    borderColor: "lightgrey"
  },
  column: {
    marginHorizontal: aspectRatio < 1.6 ? normalize(30) : normalize(10),
    flexDirection: "column",
    justifyContent: "center"
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  name: {
    color: color.Main,
    marginVertical: normalize(-1),
    fontSize: normalize(14)
  },
  detail: {
    color: color.SubMain,
    fontSize: aspectRatio < 1.6 ? normalize(14) : normalize(12)
  }
});

AppRegistry.registerComponent("CareContactScreen", () => CareContactScreen);
