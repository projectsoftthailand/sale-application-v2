import React from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
  View,
  Text
} from "react-native";

import User from "../mobx/user";
import HeadBar from "../components/HeadBar";
import normalize from "../assets/normalize";
import { get, url } from "../services/index";

const { height, width } = Dimensions.get("window");

export default class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      releaseProduct: [],
      popularProduct: []
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("release_product", User.token).then(res => {
      this.setState({
        releaseProduct: res.result
      });
    });
    await get("most_popular_product", User.token).then(res => {
      this.setState({
        popularProduct: res.result
      });
    });
  };

  _onPressItem = (id, img, name, type, description) => {
    this.props.navigation.navigate("ProductDetail", {
      id: id,
      img: img,
      name: name,
      type: type,
      description: description
    });
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <ScrollView style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <Image style={styles.banner} source={require("../assets/image/home_banner.png")} />
        <HeadBar eng={"new"} th={"สินค้าใหม่"} />
        {typeof this.state.releaseProduct !== "undefined" ? (
          <View style={styles.flexWrap}>
            {this.state.releaseProduct.map((item, i) => (
              <TouchableOpacity
                key={i}
                style={styles.item}
                onPress={() =>
                  this._onPressItem(
                    item.id_product,
                    url + "product/" + item.product_img,
                    item.product_name,
                    item.type,
                    item.product_detail
                  )
                }
              >
                <Image
                  style={styles.img}
                  source={{
                    uri: url + "product/" + item.product_img
                  }}
                />
              </TouchableOpacity>
            ))}
          </View>
        ) : (
          <View style={styles.emtryView}>
            <Text style={styles.emtryText}>ไม่มีสินค้า</Text>
          </View>
        )}

        <HeadBar eng={"popular"} th={"สินค้ายอดนิยม"} />
        {typeof this.state.popularProduct !== "undefined" ? (
          <View style={styles.flexWrap}>
            {this.state.popularProduct.map((item, i) => (
              <TouchableOpacity
                key={i}
                style={styles.item}
                onPress={() =>
                  this._onPressItem(
                    item.id_product,
                    url + "product/" + item.product_img,
                    item.product_name,
                    item.product_detail
                  )
                }
              >
                <Image
                  style={styles.img}
                  source={{
                    uri: url + "product/" + item.product_img
                  }}
                />
                {i === 0 ? <Image style={styles.popular} source={require("../assets/image/popular1.png")} /> : null}
                {i === 1 ? <Image style={styles.popular} source={require("../assets/image/popular2.png")} /> : null}
                {i === 2 ? <Image style={styles.popular} source={require("../assets/image/popular3.png")} /> : null}
              </TouchableOpacity>
            ))}
          </View>
        ) : (
          <View style={styles.emtryView}>
            <Text style={styles.emtryText}>ไม่มีสินค้า</Text>
          </View>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    height: width / 3,
    width: width / 3,
    borderColor: "lightgrey",
    borderWidth: normalize(0.2),
    zIndex: 0
  },
  popular: {
    position: "absolute",
    top: normalize(5),
    right: normalize(5),
    height: width / 12,
    width: width / 12,
    resizeMode: "contain",
    zIndex: 2
  },
  flexWrap: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  img: {
    height: width / 3,
    width: width / 3,
    resizeMode: "cover"
  },
  banner: {
    height: width / 2.25,
    width: width,
    resizeMode: "cover",
    marginBottom: normalize(-0.5)
  },
  emtryView: {
    height: width / 3,
    width: width,
    justifyContent: "center",
    alignItems: "center"
  },
  emtryText: {
    color: "lightgrey",
    fontSize: normalize(12)
  }
});

AppRegistry.registerComponent("Dashboard", () => Dashboard);
