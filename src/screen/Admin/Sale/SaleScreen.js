import React from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  StyleSheet,
  Dimensions,
  FlatList,
  Platform,
  Image,
  View,
  Text
} from "react-native";
import { SearchBar } from "react-native-elements";

import normalize from "../../../assets/normalize";
import { get, url } from "../../../services/index";
import font from "../../../assets/font";
import User from "../../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class SaleScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: true
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_sale", User.token).then(res => {
      let data = res.result;
      data.forEach(el => {
        if (el.position === null) {
          el.position = "ไม่ระบุ";
        }
        if (el.phone === null) {
          el.phone = "ไม่ระบุ";
        }
      });
      this.setState({ data: data });
    });
  };

  _onPressItem = (id, img, role, phone, lastname, firstname) => {
    this.props.navigation.navigate("SaleDetail", {
      id: id,
      img: img,
      role: role,
      phone: phone,
      lastname: lastname,
      firstname: firstname
    });
  };

  _setSearchText(event) {
    let data = sale;
    let searchText = event.trim().toLowerCase();
    let res = data.filter(l => {
      return (l.firstname.toLowerCase() + " " + l.lastname.toLowerCase()).match(searchText);
    });
    this.setState({ data: res });
  }

  _renderItem = item => {
    return (
      <TouchableOpacity
        style={styles.listContainer}
        onPress={() =>
          this._onPressItem(
            item.user_id,
            url + "user/profile/" + item.picture,
            item.position,
            item.phone,
            item.last_name,
            item.name
          )
        }
      >
        <View
          style={{
            flexDirection: "row"
          }}
        >
          <View style={styles.imgView}>
            <Image style={styles.img} source={{ uri: url + "user/profile/" + item.picture }} />
          </View>
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <Text style={[styles.name, { marginVertical: normalize(-3) }]}>
              {item.name} {item.last_name}
            </Text>
            <Text style={[styles.detail, { marginVertical: normalize(-3) }]}>
              {item.position}
              <Image style={styles.icon} source={require("../../../assets/icon_mobile/line.png")} />
              {item.phone}
            </Text>
          </View>
        </View>
        <View style={styles.iconView}>
          <Image style={styles.icon} source={require("../../../assets/icon_mobile/arrow_right.png")} />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <SearchBar
          round
          placeholder="ค้นหา"
          onChangeText={text => this._setSearchText(text)}
          onClearText={null}
          inputStyle={{
            backgroundColor: "#eee",
            fontFamily: font.medium,
            fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(14)
          }}
          containerStyle={{
            backgroundColor: "white",
            borderTopColor: "lightgrey",
            borderBottomColor: "white"
          }}
        />
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => this._renderItem(item)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    flexDirection: "row",
    borderBottomWidth: 0.5,
    borderColor: "lightgrey",
    justifyContent: "space-between"
  },
  imgView: {
    backgroundColor: "white",
    marginVertical: normalize(5),
    marginHorizontal: normalize(15),
    borderRadius: Platform.OS === "ios" ? normalize(25) : normalize(50),
    shadowOpacity: 0.5,
    shadowColor: "lightgrey",
    shadowOffset: { height: 5, width: 0 }
  },
  img: {
    backgroundColor: "white",
    height: normalize(50),
    width: normalize(50),
    borderRadius: Platform.OS === "ios" ? normalize(25) : normalize(50),
    resizeMode: "cover"
  },
  name: {
    color: color.Main,
    fontSize: normalize(16)
  },
  detail: {
    color: color.SubMain,
    fontSize: normalize(14)
  },
  iconView: {
    justifyContent: "center",
    marginHorizontal: normalize(10)
  },
  icon: {
    height: normalize(20),
    width: normalize(20),
    resizeMode: "contain"
  }
});

AppRegistry.registerComponent("SaleScreen", () => SaleScreen);
