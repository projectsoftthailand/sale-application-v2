import React, { Component } from "react";
import { TouchableOpacity, AppRegistry, StyleSheet, Dimensions, Alert, Image, View, Text } from "react-native";
import ImagePicker from "react-native-image-picker";
import LinearGradient from "react-native-linear-gradient";
import { FormLabel, FormInput } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { postForm } from "../../../services/index";
import normalize from "../../../assets/normalize";
import color from "../../../statics/color";
import font from "../../../assets/font";
import User from "../../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class SaleDetailScreen extends Component {
  constructor() {
    super();
    this.state = {
      filePic: null,
      avatarSource: null,
      name: "",
      lastname: "",
      username: ""
    };
  }

  AddPhoto = () => {
    const options = {
      title: "Select Saleman Photo",
      quality: 1.0,
      maxWidth: 256,
      maxHeight: 256,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        console.log("User selected a file form camera or gallery", response);
        this.setState({
          filePic: response,
          avatarSource: "data:image/jpeg;base64," + response.data
        });
      }
    });
  };

  _createSale = async () => {
    let data = new FormData();
    let { filePic, name, lastname, username } = this.state;
    filePic
      ? data.append("fileData", {
          uri: filePic.uri,
          type: "image/jpg",
          name: filePic.fileName
        })
      : null;
    data.append("name", name);
    data.append("lastname", lastname);
    data.append("username", username);

    try {
      await postForm("user/user_create", data, User.token).then(res => {
        if (res.success) {
          this.props.navigation.goBack();
          Alert.alert("สำเร็จ", res.message);
        } else {
          Alert.alert("ผิดพลาด", res.error_message ? res.error_message : res.message);
        }
      });
    } catch (err) {
      alert(err);
    }
  };

  render() {
    return (
      <KeyboardAwareScrollView style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <View style={styles.headContainer}>
          <View
            style={{
              shadowOpacity: 0.5,
              shadowColor: "lightgrey",
              shadowOffset: { height: 5, width: 0 }
            }}
          >
            <Image
              style={styles.img}
              source={
                this.state.avatarSource
                  ? { uri: this.state.avatarSource }
                  : require("../../../assets/image/profile_default.png")
              }
            />
          </View>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={[color.Main, color.SubMain]}
          style={{
            height: normalize(80)
          }}
        />
        <View
          style={{
            height: normalize(80)
          }}
        />
        <TouchableOpacity style={styles.button} onPress={this.AddPhoto}>
          <Image style={styles.icon} source={require("../../../assets/icon/icon_picture_white.png")} />
          <Text
            style={{
              fontSize: normalize(14),
              color: "white"
            }}
          >
            เลือกโปรไฟล์
          </Text>
        </TouchableOpacity>
        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>ชื่อจริง</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกชื่อจริง"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => this.setState({ name: res })}
          />
          <FormLabel labelStyle={styles.label}>นามสกุล</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกนามสกุล"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => this.setState({ lastname: res })}
          />
          <FormLabel labelStyle={styles.label}>ชื่อผู้ใช้</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอก Username ที่ใช้ในการเข้าสู่ระบบ"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => this.setState({ username: res })}
          />
        </View>
        <TouchableOpacity style={[styles.button, { width: width * 0.6 }]} onPress={() => this._createSale()}>
          <Text
            style={{
              fontSize: normalize(14),
              color: "white"
            }}
          >
            เพิ่มพนักงาน
          </Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headContainer: {
    height: normalize(160),
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    position: "absolute",
    width: width,
    zIndex: 99
  },
  containerInput: {
    
    marginVertical: aspectRatio < 1.6 ? normalize(5) : normalize(-10),
    borderBottomColor: color.SubMain
  },
  inputView: {
    // display: "flex",
    // alignSelf: "center",
    ...(aspectRatio < 1.6
      ? {
          paddingTop: normalize(10),
          paddingBottom: normalize(20)
        }
      : {
          paddingTop: normalize(5),
          paddingBottom: normalize(20)
        })
  },
  img: {
    width: normalize(130),
    height: normalize(130),
    borderRadius: normalize(65),
    borderWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  icon: {
    marginRight: normalize(10),
    width: normalize(20),
    height: normalize(20),
    resizeMode: "contain"
  },
  button: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: color.Main,
    padding: normalize(10),
    width: width * 0.4,
    borderRadius: normalize(50)
  },
  label: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  }
});

AppRegistry.registerComponent("SaleDetailScreen", () => SaleDetailScreen);
