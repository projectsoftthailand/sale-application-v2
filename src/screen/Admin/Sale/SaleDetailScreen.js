import React, { Component } from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  StyleSheet,
  Dimensions,
  Image,
  View,
  Text
} from "react-native";
import call from "react-native-phone-call";
import LinearGradient from "react-native-linear-gradient";
import { TabView, TabBar } from "react-native-tab-view";

import normalize from "../../../assets/normalize";
import { post } from "../../../services/index";
import color from "../../../statics/color";
import User from "../../../mobx/user";

import StatusScreen from "./StatusScreen";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
let dataClose = [];
let dataWarm = [];
let dataCool = [];
let dataHot = [];

export default class SaleDetailScreen extends Component {
  constructor() {
    super();
    this.state = {
      index: 0,
      routes: [
        {
          key: "0",
          title: "เร่งปิดการขาย",
          icon: require("../../../assets/icon/hot_active.png")
        },
        {
          key: "1",
          title: "การขายปานกลาง",
          icon: require("../../../assets/icon/warm_active.png")
        },
        {
          key: "2",
          title: "การขายปกติ",
          icon: require("../../../assets/icon/cold_active.png")
        },
        {
          key: "3",
          title: "ปิดการขาย",
          icon: require("../../../assets/icon/closing_active.png")
        }
      ],
      loading: true
    };
  }

  componentWillMount = () => {
    dataClose = [];
    dataWarm = [];
    dataCool = [];
    dataHot = [];
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    object = { sale_id: this.props.navigation.getParam("id") };
    await post("get_status_by_sale", object, User.token).then(res => {
      let data = res.result;
      if (data) {
        data.forEach((el, i) => {
          if (el.close_job === 1) {
            dataClose.unshift(el);
          } else if (el.status_name === "hot") {
            dataHot.unshift(el);
          } else if (el.status_name === "warm") {
            dataWarm.unshift(el);
          } else if (el.status_name === "cool") {
            dataCool.unshift(el);
          }
        });
      }
    });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "0":
        return <StatusScreen data={dataHot} />;
      case "1":
        return <StatusScreen data={dataWarm} />;
      case "2":
        return <StatusScreen data={dataCool} />;
      case "3":
        return <StatusScreen data={dataClose} />;
      default:
        return null;
    }
  };

  _renderIcon = ({ route }) => {
    return <Image style={styles.iconStatus} source={route.icon} />;
  };

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        renderIcon={this._renderIcon}
        labelStyle={{
          marginHorizontal: normalize(0),
          fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(9),
          color: color.Main
        }}
        indicatorStyle={{
          backgroundColor: color.Main,
          height: normalize(3)
        }}
        tabStyle={{
          marginBottom: aspectRatio < 1.6 ? normalize(-5) : normalize(-10)
        }}
        style={{
          borderBottomWidth: normalize(1),
          borderTopWidth: normalize(1),
          backgroundColor: "white",
          borderColor: "#eee",
          elevation: 1
        }}
      />
    );
  };

  render() {
    const { navigation } = this.props;
    const img = navigation.getParam("img");
    const role = navigation.getParam("role");
    const phone = navigation.getParam("phone");
    const lastname = navigation.getParam("lastname");
    const firstname = navigation.getParam("firstname");
    const args = {
      number: phone,
      prompt: false
    };

    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <View
          style={{
            height: normalize(120),
            justifyContent: "space-between",
            alignSelf: "center",
            position: "absolute",
            flexDirection: "row",
            width: aspectRatio < 1.6 ? width * 0.9 : width,
            zIndex: 99
          }}
        >
          <View
            style={{
              shadowOpacity: 0.5,
              shadowColor: "lightgrey",
              shadowOffset: { height: 5, width: 0 }
            }}
          >
            <Image style={styles.img} source={{ uri: img }} />
          </View>
          <TouchableOpacity style={styles.iconView} onPress={() => call(args).catch(console.error)}>
            <Image style={styles.iconPhone} source={require("../../../assets/icon_mobile/phone.png")} />
          </TouchableOpacity>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={[color.Main, color.SubMain]}
          style={{
            height: normalize(60)
          }}
        >
          <Text
            style={{
              marginLeft: aspectRatio < 1.6 ? normalize(150) : normalize(115),
              marginTop: normalize(30),
              fontSize: normalize(16),
              color: "white"
            }}
          >
            {firstname} {lastname}
          </Text>
        </LinearGradient>
        <View
          style={{
            flexDirection: "row",
            height: normalize(60)
          }}
        >
          <Text
            style={{
              marginLeft: aspectRatio < 1.6 ? normalize(150) : normalize(115),
              marginTop: normalize(10),
              fontSize: normalize(12),
              color: color.SubMain
            }}
          >
            {role}
            <Image style={styles.icon} source={require("../../../assets/icon_mobile/line.png")} />
            {phone}
          </Text>
        </View>
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this._renderScene}
          onIndexChange={index => this.setState({ index })}
          swipeEnabled={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    backgroundColor: "white",
    margin: normalize(15),
    width: normalize(90),
    height: normalize(90),
    borderWidth: normalize(0.5),
    borderRadius: normalize(45),
    borderColor: "lightgrey",
    resizeMode: "cover"
  },
  iconView: {
    justifyContent: "center",
    backgroundColor: color.Main,
    height: normalize(50),
    width: normalize(50),
    marginTop: normalize(35),
    marginRight: normalize(10),
    borderRadius: normalize(25)
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconPhone: {
    height: normalize(20),
    width: normalize(20),
    resizeMode: "contain",
    alignSelf: "center"
  },
  iconStatus: {
    marginBottom: normalize(-4),
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  }
});

AppRegistry.registerComponent("SaleDetailScreen", () => SaleDetailScreen);
