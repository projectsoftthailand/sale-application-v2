import React, { Component } from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  StyleSheet,
  Dimensions,
  Image,
  View,
  Text
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { TabView, TabBar } from "react-native-tab-view";

import normalize from "../../assets/normalize";
import { post } from "../../services/index";
import color from "../../statics/color";
import User from "../../mobx/user";

import StatusScreen from "./StatusScreen";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
let dataWaiting = [];
let dataWarm = [];
let dataCool = [];
let dataHot = [];

export default class ProductDetailScreen extends Component {
  constructor() {
    super();
    this.state = {
      index: 0,
      fetch: false,
      routes:
        User.role === "admin"
          ? [
              {
                key: "0",
                title: "รอการติดต่อ",
                icon: require("../../assets/icon/wait_active.png")
              },
              {
                key: "1",
                title: "เร่งปิดการขาย",
                icon: require("../../assets/icon/hot_active.png")
              },
              {
                key: "2",
                title: "การขายปานกลาง",
                icon: require("../../assets/icon/warm_active.png")
              },
              {
                key: "3",
                title: "การขายปกติ",
                icon: require("../../assets/icon/cold_active.png")
              }
            ]
          : [
              {
                key: "1",
                title: "เร่งปิดการขาย",
                icon: require("../../assets/icon/hot_active.png")
              },
              {
                key: "2",
                title: "การขายปานกลาง",
                icon: require("../../assets/icon/warm_active.png")
              },
              {
                key: "3",
                title: "การขายปกติ",
                icon: require("../../assets/icon/cold_active.png")
              }
            ],
      loading: true
    };
  }

  componentWillMount = () => {
    dataWaiting = [];
    dataWarm = [];
    dataCool = [];
    dataHot = [];
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    object = { product_id: this.props.navigation.getParam("id") };
    if (User.role === "admin") {
      await post("get_interest_by_product", object, User.token).then(res => {
        let data = res.result;
        data.forEach(el => {
          if (el.position === null) {
            el.position = "ไม่ระบุ";
          }
          if (el.status_name === "wait") {
            dataWaiting.unshift(el);
          } else if (el.status_name === "hot") {
            dataHot.unshift(el);
          } else if (el.status_name === "warm") {
            dataWarm.unshift(el);
          } else if (el.status_name === "cool") {
            dataCool.unshift(el);
          }
        });
      });
    } else {
      await post("get_interest_by_sale", object, User.token).then(res => {
        let data = res.result;
        data.forEach(el => {
          if (el.close_job === 0) {
            if (el.position === null) {
              el.position = "ไม่ระบุ";
            }
            if (el.status_name === "wait") {
              dataWaiting.unshift(el);
            } else if (el.status_name === "hot") {
              dataHot.unshift(el);
            } else if (el.status_name === "warm") {
              dataWarm.unshift(el);
            } else if (el.status_name === "cool") {
              dataCool.unshift(el);
            }
          }
        });
      });
    }
  };

  _renderScene = ({ route }) => {
    if (User.role === "admin") {
      switch (route.key) {
        case "0":
          return <StatusScreen data={dataWaiting} />;
        case "1":
          return <StatusScreen data={dataHot} />;
        case "2":
          return <StatusScreen data={dataWarm} />;
        case "3":
          return <StatusScreen data={dataCool} />;
        default:
          return null;
      }
    } else {
      switch (route.key) {
        case "1":
          return <StatusScreen data={dataHot} />;
        case "2":
          return <StatusScreen data={dataWarm} />;
        case "3":
          return <StatusScreen data={dataCool} />;
        default:
          return null;
      }
    }
  };

  _renderIcon = ({ route }) => {
    return <Image style={styles.iconStatus} source={route.icon} />;
  };

  _renderTabBar = props => {
    return (
      <TabBar
        {...props}
        renderIcon={this._renderIcon}
        labelStyle={{
          marginHorizontal: normalize(0),
          fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(9),
          color: color.Main
        }}
        indicatorStyle={{
          backgroundColor: color.Main,
          height: normalize(3)
        }}
        tabStyle={{
          marginBottom: aspectRatio < 1.6 ? normalize(-5) : normalize(-10)
        }}
        style={{
          borderBottomWidth: normalize(1),
          borderTopWidth: normalize(1),
          backgroundColor: "white",
          borderColor: "#eee",
          elevation: 1
        }}
      />
    );
  };

  _goEdit(id, img, name, type, description) {
    this.props.navigation.navigate("ProductEdit", {
      id: id,
      img: img,
      name: name,
      type: type,
      description: description
    });
  }

  render() {
    const { navigation } = this.props;
    const id = navigation.getParam("id");
    const img = navigation.getParam("img");
    const name = navigation.getParam("name");
    const type = navigation.getParam("type");
    const description = navigation.getParam("description");

    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <View
          style={{
            height: normalize(120),
            width: aspectRatio < 1.6 ? width * 0.9 : width,
            justifyContent: "space-between",
            alignSelf: "center",
            position: "absolute",
            flexDirection: "row",
            zIndex: 99
          }}
        >
          <View
            style={{
              shadowOpacity: 0.5,
              shadowColor: "lightgrey",
              shadowOffset: { height: 5, width: 0 }
            }}
          >
            <Image style={styles.img} source={{ uri: img }} />
          </View>
          {User.role === "admin" ? (
            <TouchableOpacity style={styles.iconView} onPress={() => this._goEdit(id, img, name, type, description)}>
              <Image style={styles.iconEdit} source={require("../../assets/icon_mobile/edit.png")} />
            </TouchableOpacity>
          ) : (
            <View />
          )}
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={[color.Main, color.SubMain]}
          style={{
            height: normalize(60)
          }}
        >
          <Text
            style={{
              marginLeft: aspectRatio < 1.6 ? normalize(150) : normalize(115),
              marginTop: normalize(30),
              fontSize: normalize(16),
              color: "white"
            }}
          >
            {name}
          </Text>
        </LinearGradient>
        <View
          style={{
            flexDirection: "row",
            height: normalize(60)
          }}
        >
          <Text
            style={{
              marginLeft: aspectRatio < 1.6 ? normalize(150) : normalize(115),
              marginTop: normalize(10),
              fontSize: normalize(12),
              color: color.SubMain
            }}
          >
            {description}
          </Text>
        </View>
        <TabView
          navigationState={this.state}
          renderTabBar={this._renderTabBar}
          renderScene={this._renderScene}
          onIndexChange={index => this.setState({ index })}
          swipeEnabled={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    backgroundColor: "white",
    margin: normalize(15),
    width: normalize(90),
    height: normalize(90),
    borderWidth: normalize(0.5),
    borderRadius: normalize(45),
    borderColor: "lightgrey"
  },
  iconView: {
    justifyContent: "center",
    backgroundColor: color.Main,
    height: normalize(50),
    width: normalize(50),
    marginTop: normalize(35),
    marginRight: normalize(10),
    borderRadius: normalize(25)
  },
  icon: {
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  },
  iconEdit: {
    height: normalize(20),
    width: normalize(20),
    resizeMode: "contain",
    alignSelf: "center"
  },
  iconStatus: {
    marginBottom: normalize(-4),
    height: normalize(15),
    width: normalize(15),
    resizeMode: "contain"
  }
});

AppRegistry.registerComponent("ProductDetailScreen", () => ProductDetailScreen);
