import React, { Component } from "react";
import {
  TouchableOpacity,
  ActivityIndicator,
  AppRegistry,
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  View,
  Text
} from "react-native";
import { SearchBar } from "react-native-elements";

import normalize from "../../assets/normalize";
import color from "../../statics/color";
import { get, url } from "../../services/index";
import User from "../../mobx/user";

import font from "../../assets/font";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
var product = [];

export default class ProductScreen extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: []
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_product", User.token).then(res => {
      product = res.result;
      this.setState({
        data: res.result
      });
    });
  };

  _onPressItem = (id, img, name, type, description) => {
    this.props.navigation.navigate("ProductDetail", {
      id: id,
      img: img,
      name: name,
      type: type,
      description: description
    });
  };

  _setSearch(event) {
    let searchText = event.trim().toLowerCase();
    let res = product.filter(l => {
      return l.product_name.toLowerCase().match(searchText);
    });
    this.setState({ data: res });
  }

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <View style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <SearchBar
          round
          placeholder="ค้นหา"
          onChangeText={text => this._setSearch(text)}
          onClearText={null}
          inputStyle={{
            backgroundColor: "#eee",
            fontFamily: font.medium,
            fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(14)
          }}
          containerStyle={{
            backgroundColor: "white",
            borderTopColor: "lightgrey",
            borderBottomColor: "white"
          }}
        />
        <ScrollView>
          <View style={styles.flexWrap}>
            {this.state.data.map((item, i) => (
              <TouchableOpacity
                key={i}
                style={styles.item}
                onPress={() =>
                  this._onPressItem(
                    item.id_product,
                    url + "product/" + item.product_img,
                    item.product_name,
                    item.type,
                    item.product_detail
                  )
                }
              >
                <Image
                  style={styles.img}
                  source={{
                    uri: url + "product/" + item.product_img
                  }}
                />
                <View style={styles.tag}>
                  <Text numberOfLines={1} style={styles.tagName}>
                    {item.product_name}
                  </Text>
                  <Text numberOfLines={1} style={styles.tagDescription}>
                    {item.product_detail}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    margin: normalize(2.495),
    height: aspectRatio < 1.6 ? width / 3 : width / 2,
    width: aspectRatio < 1.6 ? width / 3 - normalize(5) : width / 2 - normalize(5),
    borderColor: "lightgrey",
    borderWidth: normalize(0.2),
    resizeMode: "contain",
    zIndex: 0
  },
  flexWrap: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  img: {
    height: aspectRatio < 1.6 ? (width / 3) * 0.75 : (width / 2) * 0.75,
    width: aspectRatio < 1.6 ? width / 3 - normalize(5) : width / 2 - normalize(5),
    resizeMode: "cover"
  },
  tag: {
    paddingVertical: normalize(4),
    paddingHorizontal: normalize(7.5),
    flexDirection: "column",
    backgroundColor: color.Main,
    height: aspectRatio < 1.6 ? (width / 3) * 0.25 : (width / 2) * 0.25
  },
  tagName: {
    color: "white",
    marginBottom: aspectRatio < 1.6 ? normalize(-1) : normalize(-2.5),
    fontSize: aspectRatio < 1.6 ? normalize(12) : normalize(14)
  },
  tagDescription: {
    color: color.SubMain,
    marginTop: aspectRatio < 1.6 ? normalize(-1) : normalize(-2.5),
    fontSize: aspectRatio < 1.6 ? normalize(10) : normalize(12)
  }
});

AppRegistry.registerComponent("ProductScreen", () => ProductScreen);
