import React from "react";
import { TouchableOpacity, StyleSheet, Dimensions, FlatList, View, Image, Text } from "react-native";
import call from "react-native-phone-call";
import Swipeable from "react-native-swipeable";

import { normalize } from "react-native-elements";
import { post } from "../../services/index";
import color from "../../statics/color";
import User from "../../mobx/user";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;

export default class StatusScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  componentDidMount = () => {
    this.setState({ data: this.props.data });
  };

  _logCall = async (interest_id, phoneNumber) => {
    object = {
      interest_id: interest_id,
      phoneNumber: phoneNumber,
      callType: "OUTGOING",
      callDuration: 0
    };
    await post("create_call", object, User.token);
  };

  _renderItem = item => {
    let args = {
      number: item.phone,
      prompt: false
    };

    const rightButtons = [
      <TouchableOpacity
        style={styles.rightSwipeItem}
        onPress={() => {
          call(args).catch(console.error);
          User.role === "sale" ? this._logCall(item.interest_id, item.phone) : null;
        }}
      >
        <View style={styles.swipeable}>
          <Image style={styles.iconPhone} source={require("../../assets/icon_mobile/phone.png")} />
        </View>
      </TouchableOpacity>
    ];

    return (
      <Swipeable
        rightButtonWidth={aspectRatio < 1.6 ? width * 0.15 : width * 0.2}
        rightButtons={item.phone && !isNaN(item.phone) ? rightButtons : null}
      >
        <View style={styles.listContainer}>
          <View
            style={{
              marginLeft: aspectRatio < 1.6 ? normalize(50) : normalize(10),
              justifyContent: "center",
              flexDirection: "column"
            }}
          >
            <Text style={[styles.name, { marginVertical: normalize(-1) }]}>
              {item.name} {item.last_name}
            </Text>
            <Text style={[styles.detail, { marginVertical: normalize(-1) }]}>{item.position}</Text>
          </View>
          {item.phone && !isNaN(item.phone) ? (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginRight: aspectRatio < 1.6 ? normalize(50) : normalize(10)
              }}
            >
              <Image style={styles.iconPhoneBlue} source={require("../../assets/icon_mobile/phone_blue.png")} />
              <Text style={styles.detail}>{item.phone}</Text>
            </View>
          ) : null}
        </View>
      </Swipeable>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {typeof this.state.data[0] !== "undefined" ? (
          <FlatList
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => this._renderItem(item)}
          />
        ) : (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text style={styles.detail}>ไม่มีข้อมูล</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: aspectRatio < 1.6 ? normalize(60) : normalize(50),
    borderBottomWidth: 0.5,
    backgroundColor: "white",
    borderColor: "lightgrey"
  },
  swipeable: {
    width: aspectRatio < 1.6 ? width * 0.15 : width * 0.2,
    justifyContent: "center",
    alignItems: "center"
  },
  rightSwipeItem: {
    flex: 1,
    justifyContent: "center",
    borderColor: "lightgrey",
    borderBottomWidth: 0.5,
    backgroundColor: color.Main
  },
  icon: {
    height: normalize(20),
    width: normalize(20),
    resizeMode: "contain"
  },
  iconPhone: {
    height: normalize(25),
    width: normalize(25),
    resizeMode: "contain"
  },
  iconPhoneBlue: {
    marginRight: normalize(5),
    ...(aspectRatio < 1.6
      ? {
          height: normalize(20),
          width: normalize(20)
        }
      : {
          height: normalize(12),
          width: normalize(12)
        }),
    resizeMode: "contain"
  },
  name: {
    color: color.Main,
    fontSize: aspectRatio < 1.6 ? normalize(16) : normalize(14)
  },
  detail: {
    color: color.SubMain,
    fontSize: aspectRatio < 1.6 ? normalize(14) : normalize(12)
  }
});
