import React, { Component } from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  AppRegistry,
  StyleSheet,
  Dimensions,
  Image,
  Alert,
  View,
  Text
} from "react-native";
import ImagePicker from "react-native-image-picker";
import LinearGradient from "react-native-linear-gradient";
import { Dropdown } from "react-native-material-dropdown";
import { FormLabel, FormInput } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import User from "../../mobx/user";
import font from "../../assets/font";
import color from "../../statics/color";
import normalize from "../../assets/normalize";
import { get, postForm } from "../../services/index";

const { height, width } = Dimensions.get("window");
const aspectRatio = height / width;
var category = [];

export default class ProductAddScreen extends Component {
  constructor() {
    super();
    this.state = {
      filePic: null,
      loading: true,
      productSource: null,
      product_name: "",
      product_detail: "",
      product_type: null
    };
  }

  componentWillMount = () => {
    this._fetch().then(() => this.setState({ loading: false }));
  };

  _fetch = async () => {
    await get("get_product_type", User.token).then(res => {
      category = res.result;
    });
  };

  AddPhoto = () => {
    const options = {
      title: "Select Product Photo",
      quality: 1.0,
      maxWidth: 256,
      maxHeight: 256,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        console.log("User selected a file form camera or gallery", response);
        this.setState({
          filePic: response,
          productSource: "data:image/jpeg;base64," + response.data
        });
      }
    });
  };

  _createProduct = async () => {
    let data = new FormData();
    let { filePic, product_name, product_detail, product_type } = this.state;
    data.append("fileData", {
      uri: filePic.uri,
      type: "image/jpg",
      name: filePic.fileName
    });
    data.append("product_name", product_name);
    data.append("product_detail", product_detail);
    data.append("product_type", product_type);
    try {
      await postForm("/create_product", data, User.token).then(res => {
        if (res.success) {
          this.props.navigation.goBack();
          Alert.alert("สำเร็จ", res.message);
        } else {
          Alert.alert("ผิดพลาด", res.error_message ? res.error_message : res.message);
        }
      });
    } catch (err) {
      alert(err);
    }
  };

  render() {
    return this.state.loading ? (
      <ActivityIndicator size="large" style={{ flex: 1, alignItems: "center" }} />
    ) : (
      <KeyboardAwareScrollView style={{ flex: 1, display: "flex", backgroundColor: "white" }}>
        <View style={styles.headContainer}>
          <View style={styles.imgView}>
            <Image
              style={styles.img}
              source={
                this.state.productSource
                  ? { uri: this.state.productSource }
                  : require("../../assets/image/product_default.png")
              }
            />
          </View>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={[color.Main, color.SubMain]}
          style={{ height: normalize(80) }}
        />
        <View style={{ height: normalize(80) }} />
        <TouchableOpacity style={styles.button} onPress={this.AddPhoto}>
          <Image style={styles.icon} source={require("../../assets/icon/icon_picture_white.png")} />
          <Text style={{ fontSize: normalize(14), color: "white" }}>เลือกรูป</Text>
        </TouchableOpacity>
        <View style={styles.inputView}>
          <FormLabel labelStyle={styles.label}>ชื่อสินค้า</FormLabel>
          <FormInput
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกชื่อสินค้า"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => this.setState({ product_name: res })}
          />
          <FormLabel labelStyle={styles.label}>ประเภทสินค้า</FormLabel>
          <Dropdown
            itemCount={5}
            data={category}
            fontSize={normalize(12)}
            baseColor={color.SubMain}
            floatingLabelEnabled={true}
            selectedItemColor={color.Main}
            inputContainerStyle={{ marginTop: normalize(-15), borderBottomWidth: 1 }}
            containerStyle={[
              styles.containerInput,
              {
                marginTop: normalize(-15),
                paddingHorizontal: aspectRatio < 1.6 ? normalize(10) : normalize(20)
              }
            ]}
            onCha
            dropdownMargins={{ min: normalize(25), max: normalize(50) }}
            onChangeText={(value, index, data) => this.setState({ product_type: data[index].id })}
          />
          <FormLabel labelStyle={styles.label}>รายละเอียด</FormLabel>
          <FormInput
            multiline={true}
            containerStyle={styles.containerInput}
            inputStyle={styles.input}
            placeholder="กรอกรายละเอียด"
            underlineColorAndroid={color.SubMain}
            onChangeText={res => {
              this.setState({
                product_detail: res
              });
            }}
          />
        </View>
        <TouchableOpacity
          style={[styles.button, { width: width * 0.6 }]}
          onPress={() => {
            !this.state.filePic ||
            !this.state.productSource ||
            !this.state.product_name ||
            !this.state.product_detail ||
            !this.state.product_type
              ? Alert.alert("ผิดพลาด", "กรุณากรอกข้อมูลให้ครบถ้วน")
              : this._createProduct();
          }}
        >
          <Text
            style={{
              fontSize: normalize(14),
              color: "white"
            }}
          >
            เพิ่มสินค้า
          </Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  headContainer: {
    height: normalize(160),
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    position: "absolute",
    width: width,
    zIndex: 99
  },
  containerInput: {
    marginVertical: aspectRatio < 1.6 ? normalize(5) : normalize(-10),
    borderBottomColor: color.SubMain
  },
  inputView: {
    // display: "flex",
    // alignSelf: "center",
    ...(aspectRatio < 1.6
      ? {
          paddingTop: normalize(10),
          paddingBottom: normalize(20)
        }
      : {
          paddingTop: normalize(5),
          paddingBottom: normalize(20)
        })
  },
  imgView: {
    shadowOpacity: 0.5,
    shadowColor: "lightgrey",
    shadowOffset: { height: 5, width: 5 }
  },
  img: {
    width: normalize(130),
    height: normalize(130),
    borderRadius: normalize(65),
    borderWidth: normalize(0.5),
    borderColor: "lightgrey"
  },
  icon: {
    marginRight: normalize(10),
    width: normalize(20),
    height: normalize(20),
    resizeMode: "contain"
  },
  button: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: color.Main,
    padding: normalize(10),
    width: width * 0.4,
    borderRadius: normalize(50)
  },
  label: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(14),
    color: color.SubMain
  },
  input: {
    fontWeight: null,
    fontFamily: font.medium,
    fontSize: normalize(12)
  }
});

AppRegistry.registerComponent("ProductAddScreen", () => ProductAddScreen);
