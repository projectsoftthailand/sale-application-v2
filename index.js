import { AppRegistry } from "react-native";
import App from "./App";

console.disableYellowBox = true;

AppRegistry.registerComponent("sale_application", () => App);
