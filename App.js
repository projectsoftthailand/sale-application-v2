import React, { Component } from "react";
import { MenuProvider } from "react-native-popup-menu";
import { createStackNavigator } from "react-navigation";
import { StyleSheet, Text, View } from "react-native";
import GlobalFont from "react-native-global-font";

import ResetPasswordScreen from "./src/screen/ResetPasswordScreen";
import HeaderBackButton from "./src/components/HeaderBackButton";
import SplashScreen from "./src/screen/SplashScreen";
import LoginScreen from "./src/screen/LoginScreen";
import AdminDrawer from "./src/route/admin";
import SaleDrawer from "./src/route/sale";

import font from "./src/assets/font";

const navigationOptions = {
  header: null,
  drawerLockMode: "locked-closed",
  gesturesEnabled: false,
  headerMode: "none"
};

const LoginStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions
  },
  ResetPassword: {
    screen: ResetPasswordScreen,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderBackButton navigation={navigation} />,
      headerTitle: <Text style={styles.header}>รีเซ็ตรหัสผ่าน</Text>,
      headerRight: <View />,
      headerStyle: {
        backgroundColor: "white",
        height: normalize(60)
      }
    })
  },
  initialRouteName: "Login",
  headerMode: "none"
});

const Route = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen,
      navigationOptions
    },
    Login: {
      screen: LoginStack,
      navigationOptions
    },
    Admin: {
      screen: AdminDrawer,
      navigationOptions
    },
    Sale: {
      screen: SaleDrawer,
      navigationOptions
    }
  },
  {
    initialRouteName: "Splash",
    headerMode: "none"
  }
);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    let fontMedium = font.medium;
    GlobalFont.applyGlobal(fontMedium);
  }

  render() {
    return (
      <MenuProvider>
        <Route />
      </MenuProvider>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    position: "absolute",
    left: 0,
    right: 0,
    fontSize: normalize(16),
    fontFamily: font.bold,
    color: color.Main,
    fontWeight: "400",
    textAlign: "center"
  }
});
